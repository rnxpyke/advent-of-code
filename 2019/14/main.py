#!/bin/env python3

import re
import sys

def parse_rule(line):
    target = re.search(r'=> (\d+) (\S+)', line)
    sources = re.findall(r'(\d+) (\S+?)(?:,| =>)', line)
    return (target.group(2), (int(target.group(1)), [(int(num), typ) for num, typ in sources]))

def get_rules(lines):
    reactions = (m for line in lines if (m := parse_rule(line)) is not None)
    return { result: sources for result, sources in reactions }


def ore_of(rules, fuel):
    orders = { k: 0 for k, v in rules.items() }
    orders["FUEL"] -= fuel
    ore_cost, run = 0, True
    while run:
        run = False
        for element, count in orders.items():
            if element == "ORE": ore_cost += count
            elif count < 0:
                run = True
                num, sources = rules[element]
                factor = (-count - 1) // num + 1
                for x, source in sources:
                    if source == 'ORE': ore_cost += x * factor
                    else: orders[source] -= x * factor
                orders[element] += factor * num
    return ore_cost


if __name__ == '__main__':
    rules = get_rules(open(sys.argv[1]))
    ore_cost = ore_of(rules, 1)
    print('1 fuel:', ore_cost)

    trill = 1000000000000
    start = 1000000000000 // ore_cost
    end   = 1300000000000 // ore_cost
    while (start <= end):
        mid = (start + end) // 2
        cost = ore_of(rules, mid)
        if cost == trill: break
        if cost > trill: end = mid - 1
        else: start = mid + 1
    # print(mid, ore_of(rules, mid))
    print(mid - 1, 'fuel:', ore_of(rules, mid - 1))
