import sys
from functools import cache

def inp_empty(inp):
	return '#' not in inp

def inp_range(inp, n):
	if len(inp) < n: return None
	for x in range(n):
		if inp[x] == '.': return None
	return inp[n:]

def solve_empty(inp, ns):
	if inp == "": return solve(inp[1:], ns)
	if inp[0] == '#': return 0
	return solve(inp[1:], ns)

def solve_range(inp, ns):
	if len(ns) == 0: return 0
	if len(inp) < ns[0]: return 0
	for x in range(ns[0]):
		if inp[x] == '.': return 0
	return solve_empty(inp[ns[0]:], ns[1:])

@cache
def solve(inp, ns):
	if len(inp) < (sum(ns) + len(ns) - 1): return 0
	if inp == "": return int(len(ns) == 0)
	count = 0
	count += solve_empty(inp, ns)
	count += solve_range(inp, ns)
	return count


def parse_input(line):
	[inp, other] = line.split(' ')
	ns = tuple(int(n) for n in other.split(','))
	return (inp, ns)

assert 1 == solve(*parse_input("???.### 1,1,3"))
assert 4 == solve(*parse_input(".??..??...?##. 1,1,3"))
assert 1 == solve(*parse_input("?#?#?#?#?#?#?#? 1,3,1,6"))
assert 1 == solve(*parse_input("????.#...#... 4,1,1"))
assert 4 == solve(*parse_input("????.######..#####. 1,6,5"))
assert 10 == solve(*parse_input("?###???????? 3,2,1"))

def solve2(inp, ns):
	inp2 = "?".join([inp]*5)
	ns2 = ns*5
	a= solve(inp2, ns2)
	return a

if __name__ == '__main__':
	lines = open(sys.argv[1]).read().strip().split('\n')
	inputs = [parse_input(line) for line in lines]
	print(sum(solve(inp, ns) for inp, ns in inputs))
	
	print(sum(solve2(inp, ns) for inp, ns in inputs))
