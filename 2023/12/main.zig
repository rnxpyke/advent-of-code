const std = @import("std");
const ArrayList = std.ArrayList;

const expect = @import("std").testing.expect;
fn assert_example(springs: []const u8, counts: []const u64, expected: u64) !void {
    const res = solve_instance(springs, counts);
    try expect(res == expected);
}

const Instance = struct {
    springs: []const u8,
    counts: ArrayList(u64),
};

const ParseInstanceError = error{
    LineEmpty,
};

fn dot(springs: []const u8, counts: []const u64) u64 {
    if (springs.len == 0) {
        return solve_instance(springs, counts);
    }
    if (springs[0] == '#') {
        return 0;
    }
    return solve_instance(springs[1..], counts);
}

fn hashtag(springs: []const u8, counts: []const u64) u64 {
    if (counts.len == 0) {
        return 0;
    }
    const count = counts[0];
    if (springs.len < count) {
        return 0;
    }
    var i: usize = 0;
    while (i < count) : (i += 1) {
        if (springs[i] == '.') {
            return 0;
        }
    }
    return dot(springs[count..], counts[1..]);
}

fn solve_instance(springs: []const u8, counts: []const u64) u64 {
    if (springs.len == 0) {
        return if (counts.len == 0) 1 else 0;
    }
    return dot(springs, counts) + hashtag(springs, counts);
}

const Solver = struct {
    alloc: std.mem.Allocator,
    scratch: []u64,
    springs: []const u8,
    counts: []const u64,
    pub fn init(alloc: std.mem.Allocator, springs: []const u8, counts: []const u64) !Solver {
        const scratch: []u64 = try alloc.alloc(u64, (springs.len + 1) * (counts.len + 1));
        for (0..scratch.len) |i| {
            scratch[i] = 0;
        }
        const solver = Solver{ .alloc = alloc, .scratch = scratch, .springs = springs, .counts = counts };
        return solver;
    }

    pub fn deinit(self: *Solver) void {
        self.alloc.free(self.scratch);
    }

    pub inline fn index(self: *Solver, springs: []const u8, counts: []const u64) usize {
        const width = self.springs.len + 1;
        return springs.len + counts.len * width;
    }

    pub inline fn lookup(self: *Solver, springs: []const u8, counts: []const u64) u64 {
        const i = self.index(springs, counts);
        return self.scratch[i];
    }

    pub inline fn dot(self: *Solver, springs: []const u8, counts: []const u64) u64 {
        if (springs.len == 0) {
            return solve_instance(springs, counts);
        }
        if (springs[0] == '#') {
            return 0;
        }
        return self.lookup(springs[1..], counts);
    }

    pub inline fn hashtag(self: *Solver, springs: []const u8, counts: []const u64) u64 {
        if (counts.len == 0) {
            return 0;
        }
        const count = counts[0];
        if (springs.len < count) {
            return 0;
        }
        var i: usize = 0;
        while (i < count) : (i += 1) {
            if (springs[i] == '.') {
                return 0;
            }
        }
        return self.dot(springs[count..], counts[1..]);
    }

    pub fn solve(self: *Solver, springs: []const u8, counts: []const u64) u64 {
        if (springs.len == 0) {
            return if (counts.len == 0) 1 else 0;
        }
        return self.dot(springs, counts) + self.hashtag(springs, counts);
    }

    pub fn solveAll(self: *Solver) u64 {
        for (0..self.springs.len + 1) |s| {
            const springs = self.springs[(self.springs.len - s)..];
            for (0..self.counts.len + 1) |c| {
                const counts = self.counts[(self.counts.len - c)..];
                const res = self.solve(springs, counts);
                self.scratch[self.index(springs, counts)] = res;
            }
        }
        return self.lookup(self.springs, self.counts);
    }
};

fn solve_instance_dyn(alloc: std.mem.Allocator, springs: []const u8, counts: []const u64) !u64 {
    var solver = try Solver.init(alloc, springs, counts);
    defer solver.deinit();
    return solver.solveAll();
}

test "solve_examples" {
    try assert_example("???.###", &[_]u64{ 1, 1, 3 }, 1);
    try assert_example(".??..??...?##.", &[_]u64{ 1, 1, 3 }, 4);
}

fn parse_line(allocator: std.mem.Allocator, line: []const u8) !Instance {
    if (line.len == 0) {
        return error.LineEmpty;
    }
    var sections = std.mem.split(u8, line, " ");
    const springs = sections.next() orelse @panic("no springs");
    const counts = sections.next() orelse @panic("no counts");
    var list = ArrayList(u64).init(allocator);
    var counts_iter = std.mem.split(u8, counts, ",");
    while (counts_iter.next()) |count| {
        const count_i = try std.fmt.parseInt(u64, count, 10);
        try list.append(count_i);
    }
    return Instance{
        .springs = springs,
        .counts = list,
    };
}

fn big_input(allocator: std.mem.Allocator, springs: []const u8, counts: []const u64) !Instance {
    var lsprings = try ArrayList(u8).initCapacity(allocator, springs.len * 5 + 4);
    var lcounts = try ArrayList(u64).initCapacity(allocator, counts.len * 5);
    var writer = lsprings.writer();
    try writer.writeAll(springs);
    inline for (0..4) |_| {
        try writer.writeByte('?');
        try writer.writeAll(springs);
    }
    const long_springs = try lsprings.toOwnedSlice();
    inline for (0..5) |_| {
        try lcounts.appendSlice(counts);
    }
    return Instance{ .springs = long_springs, .counts = lcounts };
}

fn solve_input(allocator: std.mem.Allocator, input: []const u8) !u64 {
    var sum: u64 = 0;
    var lines = std.mem.split(u8, input, "\n");
    var arena = std.heap.ArenaAllocator.init(allocator);
    defer arena.deinit();
    while (lines.next()) |line| {
        _ = arena.reset(std.heap.ArenaAllocator.ResetMode.retain_capacity);
        const alloc = arena.allocator();

        if (line.len == 0) {
            continue;
        }
        const instance = try parse_line(alloc, line);
        defer instance.counts.deinit();
        const res = try solve_instance_dyn(alloc, instance.springs, instance.counts.items);
        sum += res;
    }
    return sum;
}

fn solve_input_big(allocator: std.mem.Allocator, input: []const u8) !u64 {
    var sum: u64 = 0;
    var lines = std.mem.split(u8, input, "\n");
    var arena = std.heap.ArenaAllocator.init(allocator);
    defer arena.deinit();
    while (lines.next()) |line| {
        _ = arena.reset(std.heap.ArenaAllocator.ResetMode.retain_capacity);
        const alloc = arena.allocator();

        if (line.len == 0) {
            continue;
        }
        const instance = try parse_line(alloc, line);
        defer instance.counts.deinit();
        const big = try big_input(alloc, instance.springs, instance.counts.items);
        defer alloc.free(big.springs);
        defer big.counts.deinit();
        const res = try solve_instance_dyn(alloc, big.springs, big.counts.items);
        sum += res;
    }
    return sum;
}

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = gpa.allocator();
    const stdout = std.io.getStdOut().writer();
    var args = std.process.args();
    _ = args.skip();
    const path = args.next() orelse @panic("no file arg");
    var cwd = std.fs.cwd();
    const input = try cwd.readFileAlloc(allocator, path, 1024 * 1024);
    defer allocator.free(input);

    const sum = try solve_input(allocator, input);
    try stdout.print("sum: {d}\n", .{sum});

    const sum2 = try solve_input_big(allocator, input);
    try stdout.print("sum2 {d}\n", .{sum2});
}
