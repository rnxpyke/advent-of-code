import sys

def part1(line):
    first = None
    last = None
    for c in line:
        if c in "0123456789":    
            last = c
    for c in line[::-1]:
        if c in "0123456789":
            first = c
    return int(first + last)



if __name__ == '__main__':
    lines = open(sys.argv[1]).read().strip().split('\n')
    print(sum(part1(l) for l in lines))
