import sys
from collections import Counter

def parse_point(s): return tuple(map(int, s.split(',')))

def iter_line(start, end):
    start, end = sorted([start, end])
    x1, y1 = start
    x2, y2 = end
    if x1 == x2:
        for y in range(y1, y2 + 1): yield (x1, y)
    elif y1 == y2:
        for x in range(x1, x2 + 1): yield(x, y1)
    else:
        slope = 1 if y1 < y2 else -1
        for offset in range(0, x2 - x1 + 1): yield (x1 + offset, y1 + offset * slope)

if __name__ == '__main__':
    lines = [tuple(parse_point(p) for p in line.split(' -> ')) for line in open(sys.argv[1])]
    ortho = [(s,e) for s,e in lines if s[0] == e[0] or s[1] == e[1]]

    points = Counter(p for s,e in ortho for p in iter_line(s,e))
    count  = sum(points[p] >= 2 for p in points)
    print(count)

    points = Counter(p for s,e in lines for p in iter_line(s,e))
    count = sum(points[p] >= 2 for p in points)
    print(count)
