import sys
import numpy as np

def step(grid):
    height, width = grid.shape
    nexteast = np.zeros(grid.shape, dtype=str)
    nexteast.fill('.')
    for y in range(height):
        for x in range(width):
            target = (y, (x+1)%width)
            if grid[y,x] == '>' and grid[target] == '.':
                nexteast[target] = '>'
            elif grid[y,x] != '.': nexteast[y,x] = grid[y,x]

    
    grid = nexteast
    nextdown = np.zeros(grid.shape, dtype=str)
    nextdown.fill('.')

    for y in range(height):
        for x in range(width):
            target = ((y+1)%height, x)
            if grid[y,x] == 'v' and grid[target] == '.':
                nextdown[target] = 'v'
            elif grid[y,x] != '.': nextdown[y,x] = grid[y,x]
    return nextdown

if __name__ == '__main__':
    grid = [[c for c in row.strip()] for row in open(sys.argv[1])]
    grid = np.array(grid)
    new = step(grid)
    count = 0
    while not(np.array_equal(new,grid)):
        count += 1
        grid, new = new, step(new)
    for row in grid: print("".join(row))
    print(count+1)
