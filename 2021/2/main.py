import sys

ops1 = {
    'forward': lambda hor, depth, arg: (hor+arg, depth),
    'down': lambda hor, depth, arg: (hor, depth+arg),
    'up': lambda hor, depth, arg: (hor, depth-arg),
}

ops2 = {
    'forward': lambda hor, depth, aim, arg: (hor+arg, depth+aim*arg, aim),
    'down': lambda hor, depth, aim, arg: (hor, depth, aim+arg),
    'up': lambda hor, depth, aim, arg: (hor, depth, aim-arg),
}

def run(state, insts, opcodes):
    for [op, arg] in insts: state = opcodes[op](*state, int(arg))
    return state[0] * state[1]

if __name__ == '__main__':
    values = [line.split() for line in open(sys.argv[1])]
    print(run((0,0), values, ops1))
    print(run((0,0,0), values, ops2))
