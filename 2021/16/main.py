from dataclasses import dataclass
from math import prod
import sys

@dataclass(frozen=True)
class Packet:
    version: int
    typ: int
    value: any


def header(pck):
    if len(pck) < 6: return (None, None), None
    version = int(pck[:3], 2)
    typ = int(pck[3:6], 2)
    return (version, typ), pck[6:]

def literal(pck):
    res = ""
    group, pck = pck[:5], pck[5:]
    while group[0] == '1':
        res += group[1:]
        group, pck = pck[:5], pck[5:]
    res += group[1:]
    return int(res, 2), pck

def many(parser, s):
    res = []
    v, s1 = parser(s)
    while s1 is not None:
        s = s1
        res.append(v)
        v,s1 = parser(s)
    return res, s 

def repeat(parser, num, s):
    res = []
    for _ in range(num):
        v,s = parser(s)
        res.append(v)
    return res, s

def parse_packet(pck):
    assert pck is not None
    (version, typ), pck = header(pck)
    if pck is None: return None, None
    if typ == 4:
        val, pck = literal(pck)
        assert pck is not None
        return Packet(version, typ, val), pck
    if len(pck) < 1: return None, None
    len_typ, pck = int(pck[0], 2), pck[1:]
    if len_typ == 0:
        if len(pck) < 15: return None, None
        len_bits, pck = int(pck[:15],2), pck[15:]
        subpackets,_ = many(parse_packet, pck[:len_bits])
        return Packet(version, typ, subpackets), pck[len_bits:]
    elif len_typ == 1:
        if len(pck) < 11: return None, None
        len_pck, pck = int(pck[:11], 2), pck[11:]
        assert len_pck > 0
        assert pck is not None
        subpackets, pck = repeat(parse_packet, len_pck, pck)
        return Packet(version, typ, subpackets), pck
        return None, None
    else: 
        assert False
        return None, None
    

def count_versions(pck):
    res = pck.version
    if isinstance(pck.value, list):
        res += sum(count_versions(p) for p in pck.value)
    return res

def eval_packet(pck):
    if pck.typ == 0:
        return sum(eval_packet(p) for p in pck.value)
    if pck.typ == 1:
        return prod(eval_packet(p) for p in pck.value)
    if pck.typ == 2:
        return min(eval_packet(p) for p in pck.value)
    if pck.typ == 3:
        return max(eval_packet(p) for p in pck.value)
    if pck.typ == 4:
        return pck.value
    if pck.typ == 5:
        left = eval_packet(pck.value[0])
        right = eval_packet(pck.value[1])
        return left > right
    if pck.typ == 6:
        left = eval_packet(pck.value[0])
        right = eval_packet(pck.value[1])
        return left < right
    if pck.typ == 7:
        left = eval_packet(pck.value[0])
        right = eval_packet(pck.value[1])
        return left == right
 

if __name__ == '__main__':
    pck = "".join([d for c in open(sys.argv[1]).read().strip() for d in f"{int(c,16):04b}"])
    tree,_ = parse_packet(pck)
    print(count_versions(tree))
    print(eval_packet(tree)) 
