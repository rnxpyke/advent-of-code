import numpy as np
import sys

def neighbour_idx(x,y):
    ps = [(x+1,y),(x,y+1),(x,y-1), (x-1,y)]
    return [(nx,ny) for nx,ny in ps if nx in range(w) and ny in range(h)]

def subarray(n):
    a,b = np.meshgrid([0,1,2,3,4,5,6,7,8],[0,1,2,3,4,5,6,7,8])
    return np.roll((a+b)%9+1, 1 - n, axis=0)[:5,:5]

def paths(grid):
    risc = np.zeros(grid.shape)
    risc.fill(np.inf)
    risc[0,0] = 0
    
    to_visit = np.ones(grid.shape, dtype=bool)
    while (c := np.count_nonzero(to_visit)) > 0:
        r = np.array(risc)
        r[np.logical_not(to_visit)] = np.inf
        y,x = np.unravel_index(np.argmin(r), r.shape)
        to_visit[y,x] = False
        for nx,ny in neighbour_idx(x,y):
            if to_visit[ny,nx]: risc[ny,nx] = min(risc[ny,nx], risc[y,x] + grid[ny,nx])
        if risc[-1,-1] < np.inf: break
    return risc[-1,-1]

if __name__ == '__main__':
    grid = np.array([[int(c) for c in l.strip()] for l in open(sys.argv[1])], dtype=int)
    h, w = grid.shape
    
    print(paths(grid))

    a,b = np.meshgrid([0,1,2,3,4],[0,1,2,3,4])
    grid = np.concatenate([np.concatenate([(grid+n-1)%9+1 for n in r], axis=0) for r in a+b], axis=1)
    grid = np.array(grid, dtype=int)
    h, w = grid.shape
    print(grid)

    print(paths(grid))
