import sys

matches = {"[": "]", "(": ")", "<": ">", "{": "}" }
score = { ")": 3, "]": 57, "}": 1197, ">": 25137 }

def complete(line):
    stack = []
    for c in line:
        if c in matches.keys(): stack.append(c)
        if c in matches.values(): 
            try:
                p = stack.pop()
                if matches[p] != c: return None, c
            except: return None, c
    return "".join(matches[c] for c in reversed(stack)), None

def complete_score(comp, res=0):
    for c in comp: res = res * 5 + list(score.keys()).index(c) + 1
    return res

if __name__ == '__main__':
    code = [line.strip() for line in open(sys.argv[1])]
    print(sum(score[c] for l in code if (c := complete(l)[1]) is not None))
    
    cs = sorted(complete_score(c) for l in code if (c := complete(l)[0]) is not None)
    print(cs[len(cs)//2])
