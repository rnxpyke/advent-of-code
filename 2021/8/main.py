import sys

def decode_value(i, o):
    values = sorted(["".join(sorted(v)) for v in i], key=lambda v: len(v))
    one, seven, four, eight = values[0], values[1], values[2], values[-1]
    seg2, seg1 = values[3:6], values[6:9]
    nine = [v for v in seg1 if all(c in v for c in four)][0]
    seg1.remove(nine)
    zero = [v for v in seg1 if all(c in v for c in one)][0]
    seg1.remove(zero)
    six = seg1[0]
    three = [v for v in seg2 if all(c in v for c in one)][0]
    seg2.remove(three)
    not_nine = list(set("abcdefg") - set(nine))[0]
    two = [v for v in seg2 if not_nine in v][0]
    seg2.remove(two)
    five = seg2[0]
    nums = { zero: 0, one: 1, two: 2, three: 3, four: 4, five: 5, six: 6, seven: 7, eight: 8, nine: 9}
    return int("".join(str(nums["".join(sorted(n))]) for n in o))

if __name__ == '__main__':
    inp = [[x.split(' ') for x in line.strip().split(' | ')] for line in open(sys.argv[1])]
    
    print(sum(len(v) in [2, 3, 4, 7] for _,o in inp for v in o))
    # all lines contain all numbers
    print(len(inp) == sum(len(set(v for v in i)) == 10 for i,_ in inp))
    print(sum(decode_value(*line) for line in inp))


