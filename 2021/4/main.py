import sys
import numpy as np

def parseBoard(inp):
    lines = list(inp.strip().splitlines())
    return np.array([[int(v) for v in line.split()] for line in lines])

def mark(called, board): return np.array([[v in called for v in line] for line in board])
def solved(board): return np.any(np.all(board, axis=1)) or np.any(np.all(board, axis=0))

def score(called, board):
    for i in range(1, len(called)+1):
        marked = mark(called[:i], board)
        if solved(marked):
            score = np.sum(board * np.logical_not(marked)) * called[i-1]
            return score, i
    return None, None

if __name__ == '__main__':
    called, *rest = [line.strip() for line in open(sys.argv[1])]
    called = [int(v) for v in called.split(',')]
    boards = list(map(parseBoard, "\n".join(rest).split("\n\n")))

    scores = [score(called, board) for board in boards]
    scores.sort(key=lambda k: k[1])
    print(scores[0][0])
    print(scores[-1][0])


