import sys

def quad(n): return n*(n+1) // 2

if __name__ == '__main__':
    crabs = [int(c) for line in open(sys.argv[1]) for c in line.split(',')]
    n = max(crabs)+1 
    print(*min((sum(abs(c - i) for c in crabs), i) for i in range(n)))
    print(*min((sum(quad(abs(c - i)) for c in crabs), i) for i in range(n)))
