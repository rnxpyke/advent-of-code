import sys

def task1(ms):
    return sum(b > a for a,b in zip(ms, ms[1:]))

def task2(ms):
    windows = list(zip(ms, ms[1:], ms[2:]))
    return sum(sum(w2) > sum(w1) for w1, w2 in zip(windows, windows[1:]))

if __name__ == '__main__':
    ms = [int(line) for line in open(sys.argv[1])]
    print(task1(ms))
    print(task2(ms))
