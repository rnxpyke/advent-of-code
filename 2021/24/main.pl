:- use_module(library(clpfd)).

input(Ns) :- length(Ns, 14),
	Ns ins 1..9.

initState(state{ x: 0, y: 0, z:0, w: 0 }).


lookup_or_im(V, _, V) :- number(V).
lookup_or_im(V, S, NV) :- atom(V), NV = S.V.

operation(add, A, B, R) :- R #= A + B.
operation(mul, A, B, R) :- R #= A * B.
operation(div, A, B, R) :- R #= A // B.
operation(mod, A, B, R) :- A #>= 0, B #> 0, R #= A mod B.
operation(eql, A, A, 1).
operation(eql, A, B, 0) :- A #\= B.

inst(inp(A), [I|Is], State, Is, RestState) :- RestState = State.put(A, I).
inst(I, Is, State, Is, RestState) :-
	I =.. [OP, A, B],
	L = State.A,
	lookup_or_im(B, State, R),
	operation(OP, L, R, V),
	RestState = State.put(A, V).

interpret(_, [], State0, State0).
interpret(Input, [I|Is], State0, NextState) :-
	inst(I, Input, State0, RestInput, State1),
	interpret(RestInput, Is, State1, NextState).

string_term(S,T) :- term_string(T, S).

parse_instr(S, I) :-
	split_string(S, " ", "", Words),
	maplist(string_term, Words, Terms),
	I =.. Terms.

noteof(L) :- \+ L = end_of_file.
read_program(P) :-
	open('input.txt', read, Str),
	read_string(Str, _, S),
	split_string(S, "\n", "", Lines),
	maplist(parse_instr, Lines, Instrs),
	include(noteof, Instrs, P),
	close(Str), !.


valid_input(I, P) :-
	input(I),
	initState(S),
	interpret(I, P, S, R),
	0 = R.z.

minimize_input(I,P) :-
	valid_input(I,P),
	labeling([min,up], I), !.

maximize_input(I,P) :-
	valid_input(I,P),
	labeling([max,down], I), !.

main :-
	read_program(P),
	maximize_input(I0, P),
	atomic_list_concat(I0, R0),
	write_term(R0, [nl]),

	minimize_input(I1, P),
	atomic_list_concat(I1, R1),
	write_term(R1, [nl]).
