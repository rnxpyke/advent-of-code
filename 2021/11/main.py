import numpy as np
import sys

def neighbour_idx(x, y):
    p = [(x-1,y-1),(x-1,y),(x-1,y+1),(x,y-1),(x,y+1),(x+1,y-1),(x+1,y),(x+1,y+1)]
    return [(nx,ny) for nx, ny in p if nx in range(w) and ny in range(h)]

def step(m):
    grid = m + 1
    flashed = set()
    while to := set((x,y) for x,y in np.argwhere(grid > 9)) - flashed:
        for x,y in to:
            flashed.add((x,y))
            for nx, ny in neighbour_idx(x,y):
                grid[nx][ny] += 1
    grid[grid > 9] = 0
    return grid, len(flashed)
    
if __name__ == '__main__':
    inp = np.array([[int(c) for c in line.strip()] for line in open(sys.argv[1])])
    w, h = len(inp[0]), len(inp)

    grid, count = inp, 0
    for _ in range(100):
        grid, c = step(grid)
        count += c
    print(count)

    grid = inp
    for i in range(100000):
        grid, c = step(grid)
        if c >= 100: break
    print(i+1)
