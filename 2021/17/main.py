import re
import sys

def shoot_x(vx, mx=None):
    x = 0
    while mx is None or x <= mx:
        yield x
        x += vx
        if vx != 0: vx += -1 if vx > 0 else 1

def shoot_y(vy, my=None):
    y = 0
    while my is None or y >= my:
        yield y
        y += vy
        vy -= 1

def shoot(vx, vy, mx=None, my=None):
    for x,y in zip(shoot_x(vx, mx), shoot_y(vy, my)):
        yield x,y

def valid_shot(vx, vy, xr, yr):
    for x,y in shoot(vx, vy, xr.stop, yr.start):
        if x in xr and y in yr: return True
    return False


if __name__ == '__main__':
    exp = r'.*x=(-?\d+)..(-?\d+), y=(-?\d+)..(-?\d+)'
    xs, xe, ys, ye = [int(v) for v in re.match(exp, open(sys.argv[1]).read()).groups()]
    xr = range(xs, xe+1)
    yr = range(ys, ye+1)

    x = min(x for x in range(0, 1000) if sum(range(0,x+1)) in xr)
    vy = max(y for y in range(0, 1000) if valid_shot(x, y, xr, yr))
    my = max(y for x,y in shoot(x,vy, xr.stop, yr.start))
    print(my)

    print(sum(valid_shot(x,y, xr, yr) for x in range(0, xr.stop + 1) for y in range(yr.start - 1, vy+1)))
