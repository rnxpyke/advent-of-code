import sys
import numpy as np

def bit(c): return c == '1'
def char(b): return '1' if b else '0'

def task2(numbers, choose):
    for i in range(0, 12):
        common = sum(num[i] == '1' for num in numbers) >= len(numbers) / 2
        numbers = [num for num in numbers if choose(bit(num[i]) == common)]
        if len(numbers) <= 1: return numbers[0]
        

def task1(numbers):
    nums = np.array([[1 if c == '1' else 0 for c in line] for line in numbers])
    commonbits = nums.sum(axis=0) > len(numbers)//2
    gammastring = "".join(char(b) for b in commonbits)
    epsilonstring = "".join(char(not(b)) for b in commonbits)
    return int(gammastring, 2), int(epsilonstring, 2)
 
if __name__ == '__main__':
    numbers = sorted([line.strip() for line in open(sys.argv[1])])

    gamma, epsilon = task1(numbers)
    print(gamma*epsilon)
     
    oxygen = int(task2(numbers, lambda x: x), 2)
    co2 = int(task2(numbers, lambda x: not(x)), 2)
    print(oxygen * co2)
