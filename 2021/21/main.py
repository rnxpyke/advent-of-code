import sys
from dataclasses import dataclass
from itertools import count
from collections import Counter
from collections import defaultdict

@dataclass(frozen=True)
class Player:
    pos: int
    score: int = 0

    def move(self, i):
        newpos = (self.pos + i - 1) % 10 + 1
        return Player(newpos, self.score + newpos)

@dataclass
class DetDie:
    val: int = 1
    rolls: int = 0
    def roll(self):
        self.rolls += 1
        res = self.val
        self.val = (self.val) % 100 + 1
        return res

def task1(cur, other):
    die = DetDie()
    while True:
        a,b,c = die.roll(), die.roll(), die.roll()
        cur = cur.move(a+b+c)
        if cur.score >= 1000:
            break
        cur, other = other, cur
    return die.rolls*other.score
 

if __name__ == '__main__':
    p1,p2 = [Player(int(line.split(': ')[1])) for line in open(sys.argv[1])]
    print(task1(p1,p2)) 

    throws = Counter(a+b+c for a in range(1,4) for b in range(1,4) for c in range(1,4))

    universes = defaultdict(Counter)
    universes[0].update({(p1, p2) : 1})
    wins = Counter()

    for i in range(42):
        for (p1,p2),v in universes[i-1].items():
                if p1.score < 21 and p2.score < 21:
                    for die,c in throws.items():
                        if i % 2 == 1: board = p1.move(die), p2
                        else: board = p1, p2.move(die)
                        assert not(board[0].score >= 21 and board[1].score >= 21)
                        universes[i].update({board: v*c})
                        if board[0].score >= 21: wins[1] += v*c
                        if board[1].score >= 21: wins[2] += v*c
    print(wins.most_common(1)[0][1])
