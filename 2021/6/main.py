import sys
from collections import Counter

def age(fishes, i=1):
    for _ in range(i):
        res = {k: 0 for k in range(-1,9)}
        for age in fishes: res[age-1] += fishes[age]
        res[6], res[8] = res[6] + res[-1], res[-1]
        fishes = {k: res[k] for k in range(9)}
    return fishes

if __name__ == '__main__':
    fishes = [int(f) for line in open(sys.argv[1]) for f in line.split(',')]

    print(sum(age(Counter(fishes), 80).values()))
    print(sum(age(Counter(fishes), 256).values()))
