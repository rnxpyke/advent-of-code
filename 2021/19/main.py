import sys
import re
import numpy as np
from itertools import combinations

x = np.array([1, 0, 0])
y = np.array([0, 1, 0])
z = np.array([0, 0, 1])
rotations = np.array([
  # x is facing x
  [x, y, z],
  [x, -z, y],
  [x, -y, -z],
  [x, z, -y],
  # x is facing -x
  [-x, -y, z],
  [-x, -z, -y],
  [-x, y, -z],
  [-x, z, y],
  # x is facing y
  [-z, x, -y],
  [y, x, -z],
  [z, x, y],
  [-y, x, z],
  # x is facing -y
  [z, -x, -y],
  [y, -x, z],
  [-z, -x, y],
  [-y, -x, -z],
  # x is facing z
  [-y, -z, x],
  [z, -y, x],
  [y, z, x],
  [-z, y, x],
  # x is facing -z
  [z, y, -x],
  [-y, z, -x],
  [-z, -y, -x],
  [y, -z, -x]
])

def overlap(a, b):
    mi = 0
    rot = None
    boff = None
    for i,r in enumerate(rotations):
        nb = np.matmul(b, r)
        for p1 in a:
            for p2 in nb:
                off = p1 - p2
                c = nb + off
                sc = set(tuple(p) for p in c)
                sa = a if isinstance(a, set) else set(tuple(p) for p in c)
                inter = len(sc.intersection(sa))
                if inter > mi:
                    mi = inter
                    rot = i
                    boff = off
                if inter >= 12:
                    return i, off, inter
    return rot, boff, mi

def read_scanner(s):
    head, *points = s.splitlines()
    assert re.match('--- scanner \d+ ---', head)
    return np.array([[int(c) for c in point.split(',')] for point in points])

if __name__ == '__main__':
    scanners = [read_scanner(s) for s in open(sys.argv[1]).read().split('\n\n')]
    
    cloud = set(tuple(p) for p in scanners[0])
    matched = {}
    for i in range(len(scanners) + 1):
        print(i)
        if len(matched) == len(scanners): break
        for i, a in enumerate(scanners):
            if i in matched: continue
            rot, boff, mi = overlap(cloud, a)
            if mi >= 12:
                points = np.matmul(a, rotations[rot]) + boff
                cloud.update(tuple(p) for p in points)
                matched[i] = boff
   
    
    print(len(cloud)) 
    print(matched)
    print(max(np.sum(np.absolute(a-b)) for a,b in combinations(matched.values(), 2)))
