import sys

def stack(n, depth=0):
    l = []
    l.append(('open', depth))
    for e in n:
        if isinstance(e, list):
            l.extend(stack(e,depth=depth+1))
        else:
            l.append(('im', e))
    l.append(('close', depth))
    return l

def unstack(n, depth=0):
    n.pop(0)
    l = []
    while n[0][0] != 'close':
        if n[0][0] == 'im': l.append(n.pop(0)[1])
        if n[0][0] == 'open': l.append(unstack(n, depth=depth+1))
    n.pop(0)
    if depth==0: assert len(n) == 0
    return l



def simp_once(n):
    s = stack(n)
    for idx, (op, v) in enumerate(s): #explode
        if op == 'open' and v == 4:
            assert s[idx+1][0] == 'im'
            assert s[idx+2][0] == 'im'
            assert s[idx+3][0] == 'close'
            left, right = s[idx+1][1], s[idx+2][1]
            for i in reversed(range(0, idx)):
                if s[i][0] == 'im':
                    s[i] = ('im', s[i][1] + left)
                    break
            for i in range(idx+4, len(s)):
                if s[i][0] == 'im':
                    s[i] = ('im', s[i][1] + right)
                    break
            return unstack(s[:idx] + [('im', 0)] + s[idx+4:]), False

    for idx, (op, v) in enumerate(s): #split
        if op == 'im' and v >= 10:
            return unstack(s[:idx] + stack([v//2, abs(-v//2)]) + s[idx+1:]), False            
            print('split')
    return n, True

def simp(n):
    for i in range(10000):
        n, reduced = simp_once(n)
        if reduced: break
    assert i != 9999
    return n

def add(a,b): return simp([a,b])

def fold(nums):
    acc = nums[0]
    for n in nums[1:]:
        acc = add(acc, n)
    return acc

def magnitude(n):
    if isinstance(n, list): return 3*magnitude(n[0]) + 2*magnitude(n[1])
    return n

def tests():
    assert add([1,2], [[3,4],5]) == [[1,2], [[3,4],5]]

    assert simp_once([[[[[9,8],1],2],3],4])[0] == [[[[0,9],2],3],4]
    assert simp_once([7,[6,[5,[4,[3,2]]]]])[0] == [7,[6,[5,[7,0]]]]
    assert simp_once([[6,[5,[4,[3,2]]]],1])[0] == [[6,[5,[7,0]]],3]
    assert simp_once([[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]])[0] == [[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]
    assert simp_once([[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]])[0] == [[3,[2,[8,0]]],[9,[5,[7,0]]]]

    assert add([[[[4,3],4],4],[7,[[8,4],9]]], [1,1]) == [[[[0,7],4],[[7,8],[6,0]]],[8,1]]

    assert fold([[1,1], [2,2], [3,3], [4,4]]) == [[[[1,1],[2,2]],[3,3]],[4,4]]

if __name__ == '__main__':
    numbers = [eval(l) for l in open(sys.argv[1])]
    tests()

    print(magnitude(fold(numbers)))
    print(max(magnitude(add(a,b)) for a in numbers for b in numbers))
