import sys
from collections import Counter

def neighbour_idx(x, y):
    p = [(x+1, y), (x-1, y), (x,y+1), (x,y-1)]
    return [(nx,ny) for nx,ny in p if nx in range(w) and ny in range(h)]

def is_basin(m, x, y): return all(m[y][x] < m[ny][nx] for nx,ny in neighbour_idx(x,y)) 

def bottom(m, bs, x, y):
    while (x,y) not in bs: x,y = min(neighbour_idx(x,y), key=lambda p: m[p[1]][p[0]])
    return x,y

if __name__ == '__main__':
    input = [[int(c) for c in line.strip()] for line in open(sys.argv[1])]
    h, w = len(input), len(input[0])

    basins = [(x,y) for x in range(w) for y in range(h) if is_basin(input, x, y)]
    print(sum(1 + input[y][x] for x,y in basins))

    occ = Counter(bottom(input,basins,x,y) for x in range(w) for y in range(h) if input[y][x] != 9)
    a,b,c = [c for _,c in occ.most_common(3)]
    print(a*b*c)
