from collections import Counter
import sys

def grow(pairs, rules):
    n = Counter()
    for p,v in pairs.items():
        (l, r), m = p, rules[p]
        n[l+m] += v; n[m+r] += v
    return n

def element_count(pairs):
    cs = Counter([template[0], template[-1]])
    for (l,r), v in pairs.items(): cs[l] += v; cs[r] += v
    return { k: v // 2 for k, v in cs.items() }

if __name__ == '__main__':
    template, _, *rules = [line.strip() for line in open(sys.argv[1])]
    rules = { key: value for key, value in map((lambda x: x.split(' -> ')), rules) }
    pairs = Counter(a+b for a,b in zip(template, template[1:]))
    
    for _ in range(10): pairs = grow(pairs, rules)
    counts = element_count(pairs)
    print(max(counts.values()) - min(counts.values()))

    for _ in range(30): pairs = grow(pairs, rules)
    counts = element_count(pairs)
    print(max(counts.values()) - min(counts.values()))
