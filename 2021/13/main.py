import sys
import numpy as np

def fold(grid, axis, idx):
    first, line, second = np.split(grid, [int(idx), int(idx)+1], {'x': 1, 'y': 0}[axis])
    return first + {'x': np.fliplr, 'y': np.flipud}[axis](second)

if __name__ == '__main__':
    points = np.array([[int(n) for n in l.split(',')] for l in open(sys.argv[1]) if ',' in l])
    folds = [l.strip().split(' ')[2].split('=') for l in open(sys.argv[1]) if ' ' in l]
    grid = np.zeros((max(points[:,1])+1, max(points[:,0]+1)))
    for x,y in points: grid[y,x] = 1
    
    print(np.count_nonzero(fold(grid, folds[0][0], folds[0][1])))
    
    for axis, idx in folds: grid = fold(grid, axis, idx)
    print("\n".join(["".join('#' if c > 0 else ' ' for c in row) for row in grid]))
