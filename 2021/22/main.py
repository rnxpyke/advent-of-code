import sys
import re
from dataclasses import dataclass
from collections import defaultdict
from itertools import combinations
from tqdm import tqdm

@dataclass(frozen=True)
class Vec:
    x: int
    y: int
    z: int

    def __iter__(self): yield self.x; yield self.y; yield self.z;
    def emax(self, other): return Vec(*[max(a,b) for a,b in zip(self, other)])
    def emin(self, other): return Vec(*[min(a,b) for a,b in zip(self, other)])

@dataclass(frozen=True)
class Box:
    start: Vec
    end: Vec
    
    def xrange(self): return range(self.start.x, self.end.x+1)
    def yrange(self): return range(self.start.y, self.end.y+1)
    def zrange(self): return range(self.start.z, self.end.z+1)

    def __iter__(self):
        for x in self.xrange():
            for y in self.yrange():
                for z in self.zrange():
                    yield Vec(x,y,z)

    def __contains__(self, item):
        if isinstance(item, Vec):
            return item.x in self.xrange() and item.y in self.yrange() and item.z in self.zrange()
        if isinstance(item, Box):
            return item.start in self and item.end in self
    
    def volume(self):
        x,y,z = [a+1-b for a,b in zip(self.end, self.start)]
        if x < 0: return 0
        if y < 0: return 0
        if z < 0: return 0
        return x * y * z

    def intersection(self, other):
        start = Vec(*[max(a,b) for a,b in zip(self.start, other.start)])
        end = Vec(*[min(a,b) for a,b in zip(self.end, other.end)])
        return Box(start, end)

    def intersects(self, other):
        return self.intersection(other).volume() != 0

def parse_instr(l):
    typ, *coords = re.match(r'(on|off) x=(.*),y=(.*),z=(.*)', l).groups()
    start = Vec(*[min(int(a) for a in c.split('..')) for c in coords])
    end = Vec(*[max(int(a) for a in c.split('..')) for c in coords])
    return (typ, Box(start, end))

def task1(instrs):
    init_area = Box(Vec(-50, -50, -50), Vec(50, 50, 50))
    init_instr = [i for i in instrs if i[1] in init_area]
    
    points = defaultdict(int)
    for i,b in init_instr:
        for p in b:
            points[p] = {'on': 1, 'off': 0}[i]
    return sum(points.values())

def partition(box, intersect):
    cur,step = [box], []

    # x axis
    for obj in cur:
        step.append(Box(obj.start, Vec(intersect.start.x - 1, obj.end.y, obj.end.z)))
        step.append(Box(Vec(intersect.start.x, obj.start.y, obj.start.z), Vec(intersect.end.x, obj.end.y, obj.end.z)))
        step.append(Box(Vec(intersect.end.x + 1, obj.start.y, obj.start.z), obj.end))
    cur, step = [b for b in step if b.volume() > 0], []
    assert sum(p.volume() for p in cur) == box.volume(), "Wrong Partition x"
    
    # y axis 
    for obj in cur:
        step.append(Box(obj.start, Vec(obj.end.x, intersect.start.y - 1, obj.end.z)))
        step.append(Box(Vec(obj.start.x, intersect.start.y, obj.start.z), Vec(obj.end.x, intersect.end.y, obj.end.z)))
        step.append(Box(Vec(obj.start.x, intersect.end.y + 1, obj.start.z), obj.end))
    cur, step = [b for b in step if b.volume() > 0], []
    
    assert sum(p.volume() for p in cur) == box.volume(), "Wrong Partition y"
 
    # z axis 
    for obj in cur:
        step.append(Box(obj.start, Vec(obj.end.x, obj.end.y, intersect.start.z  -1)))
        step.append(Box(Vec(obj.start.x, obj.start.y, intersect.start.z), Vec(obj.end.x, obj.end.y, intersect.end.z)))
        step.append(Box(Vec(obj.start.x, obj.start.y, intersect.end.z + 1), obj.end))
    cur, step = [b for b in step if b.volume() > 0], []

    assert sum(p.volume() for p in cur) == box.volume(), "Wrong Partition"
    return cur

def eval_instr(board, i, box):
    if i == 'on':
        to_eval = [box]
        for cube in board:
            step = []
            for part in to_eval:
                if part.intersects(cube):
                     intersect = part.intersection(cube)
                     step.extend(partition(part, intersect))
                else: step.append(part)
            to_eval = [p for p in step if p not in cube]
            if len(to_eval) == 0: break
        for part in to_eval: board.add(part)
    else:
        cubes = set()
        for cube in board:
            if not(cube.intersects(box)): cubes.add(cube)
            else:
                for split in partition(cube, cube.intersection(box)):
                    if split not in box: cubes.add(split)
        board = cubes
    return board

def task2(instrs):
    size = 1000000
    bounds = Box(Vec(-size, -size, -size), Vec(size,size,size))
    for _,box in instrs: assert box in bounds
    center = Box(Vec(-size//3, -size//3, -size//3), Vec(size//3, size//3, size//3))
    regions = partition(bounds, center)

    board = {region: set() for region in regions }

    for i, box in tqdm(instrs):
        partitions = {region: box.intersection(region) for region in regions if region.intersects(box)}
        for region, cube in partitions.items():
            board[region] = eval_instr(board[region], i, box)

    res = sum(box.volume() for region in board.values() for box in region)
    return res

def mayabox(x1,x2,y1,y2,z1,z2):
    return Box(Vec(x1,y1,z1), Vec(x2,y2,z2))

if __name__ == '__main__':
    instrs = [parse_instr(line) for line in open(sys.argv[1])]



    first = mayabox(-5, 47, -31, 22, -19, 33)
    second = mayabox(-44, 5, -27, 21, -14, 35)
    res = {(-5, 47, -31, -28, -19, 33): 'on', (-5, 47, 22, 22, -19, 33): 'on', (7, 48, -31, 22, -19, 33): 'on', (-5, 47, -31, 22, -18, -14): 'on', (-44, 5, -27, 21, -14, 35): 'on'}

    print(first.intersection(second))
    for a,b in combinations(res.keys(), 2):
        a = mayabox(*a)
        b = mayabox(*b)
        assert not(a.intersects(b)), f"\n{a}\n{b}\n{a.intersection(b)}"

    
    print(sum(mayabox(*box).volume() for box in res.keys()))
    print(first.volume() + second.volume() - first.intersection(second).volume())
    print(first.volume())

    

