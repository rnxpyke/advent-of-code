import numpy as np
import sys 

stringorder = np.array([
    [1,1], [0,1], [-1,1],
    [1,0], [0,0], [-1,0],
    [1,-1],[0,-1],[-1,-1],
])

lookup = {'#': 1, '.': 0}

def step(grid):
    zero = np.zeros(2,dtype=int)
    total = np.zeros(grid.shape, dtype=int)
    for i,offset in enumerate(stringorder):
        r = np.roll(grid, zero - offset, axis=(1,0))
        b = (1 << i) * r
        total += b
    return np.array([[table[num] for num in row] for row in total])

if __name__ == '__main__':
    table, _, *grid = [l.strip() for l in open(sys.argv[1])]
    table = [lookup[i] for i in table]
    orig = np.array([[lookup[c] for c in row] for row in grid])
    grid = np.pad(orig, 10)
    
    s1 = step(grid)
    s2 = step(s1)
    print(np.count_nonzero(s2))

    grid = np.pad(orig, 51)
    for i in range(50):
        grid = step(grid)
    print(np.count_nonzero(grid))

