import sys

def priority(char):
    if ord('a') <= ord(char) <= ord('z'): return ord(char) - ord('a') + 1
    if ord('A') <= ord(char) <= ord('Z'): return ord(char) - ord('A') + 27
    assert False
    return None

def part1_score(left, right):
    shared = set(left).intersection(set(right))
    assert len(shared) == 1
    item = list(shared)[0]
    return priority(item)

def part2_score(a,b,c):
    shared = set(a).intersection(set(b)).intersection(set(c))
    assert len(shared) == 1
    item = list(shared)[0]
    return priority(item)

if __name__ == '__main__':
    lines = open(sys.argv[1]).read().strip().split('\n')
    rucksacks = [(s[:len(s)//2], s[len(s)//2:]) for s in lines]
    print(sum(part1_score(l, r) for l, r in rucksacks))

    print(sum(part2_score(lines[i], lines[i+1], lines[i+2]) for i in range(0, len(lines), 3)))