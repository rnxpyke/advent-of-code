#!/usr/bin/env swipl

:- initialization(main, main).
:- use_module(library(clpfd)).

:- multifile clpfd:run_propagator/2.

make_resources(Tag, Ore, Clay, Obsidian, Dict) :-
    Dict = Tag{ ore: Ore, clay: Clay, obsidian: Obsidian }.

read_blueprint(Line, Blueprint) :-
    split_string(Line, " ", ":", Words),
    findall(Int, (member(M, Words), number_string(Int, M)), Ints),
    [Id, COreOre, CClayOre, CObsOre, CObsClay, CGeodeClay, CGeodeObs] = Ints,
    make_resources(cost, COreOre, 0, 0, COre),
    make_resources(cost, CClayOre, 0, 0, CClay),
    make_resources(cost, CObsOre, CObsClay, 0, CObs),
    make_resources(cost, 0, CGeodeClay, CGeodeObs, CGeode),
    Robots = bots{ore: COre, clay: CClay, obsidian: CObs, geode: CGeode},
    Blueprint = (Id = Robots).

read_program(Name, Bs) :-
    open(Name, read, File),
    read_string(File, _, S),
    split_string(S, "\n", "", Ls),
    findall(B, (member(L, Ls), read_blueprint(L, B)), Data),
    dict_create(Bs, blueprints, Data).

res_instantiated(Dict) :-
    Dict = resources{ore: _, clay: _, obsidian: _},
    \+ var(Dict.ore),
    \+ var(Dict.clay),
    \+ var(Dict.obsidian).
bots_instantiated(Dict) :-
    Dict = rebots{ore: _, clay: _, obsidian: _},
    \+ var(Dict.ore),
    \+ var(Dict.clay),
    \+ var(Dict.obsidian).

move_num(nil, 0).
move_num(ore, 1).
move_num(clay, 2).
move_num(obsidian, 3).
move_num(geode, 4).

bp_resource_maxcost(Bp, Res, Max) :-
    Max #= max(max(Bp.ore.Res, Bp.clay.Res), max(Bp.obsidian.Res, Bp.geode.Res)).
bp_maxcost(Bp, MaxCost) :-
    make_resources(maxcost, MOre, MClay, MObsidian, MaxCost),
    bp_resource_maxcost(Bp, ore, MOre),
    bp_resource_maxcost(Bp, clay, MClay),
    bp_resource_maxcost(Bp, obsidian, MObsidian).

buy_bot(B, Bot, Res, resources{ clay: Clay, ore: Ore, obsidian: Obsidian}) :-
    Ore #= Res.ore - B.Bot.ore,
    Clay #= Res.clay - B.Bot.clay,
    Obsidian #= Res.obsidian - B.Bot.obsidian,
    Ore #>= 0, Clay #>= 0, Obsidian #>= 0.

gather_resources(Bots, Res, resources{ clay: Clay, ore: Ore, obsidian: Obsidian }) :-
    Clay #= Bots.clay + Res.clay,
    Ore #= Bots.ore + Res.ore,
    Obsidian #= Bots.obsidian + Res.obsidian.

triangle(0, 0) :- !.
triangle(N, R) :- 
    Lower is N - 1,
    triangle(Lower, N1),
    R is N + N1.

do_move(Bp, Rem, geode, R, B, S, NR, B, NS) :-
    buy_bot(Bp, geode, R, SellRes),
    gather_resources(B, SellRes, NR),
    NS #= S + Rem.
do_move(Bp, _, Normal, R, B, S, NR, NB, S) :-
    member(Normal, [obsidian, clay, ore]),
    buy_bot(Bp, Normal, R, SellRes),
    gather_resources(B, SellRes, NR),
    Cnt #= B.Normal + 1,
    put_dict(Normal, B, Cnt, NB).
do_move(_, _, nil, R, B, S, NR, B, S) :- gather_resources(B, R, NR).


follow_move(Bp, Rem, M, R, B, S, NR, NB, NS) :-
    clpfd:make_propagator(follow_move(Bp, Rem, M, R, B, S, NR, NB, NS), Prop),
    clpfd:init_propagator(M, Prop),
    clpfd:init_propagator(R, Prop),
    clpfd:init_propagator(B, Prop),
    clpfd:init_propagator(S, Prop),
    clpfd:trigger_once(Prop).

move_transition_contraints(Rem, R, B, S, NR, NB, NS) :-
    triangle(Rem, RemMax),
    NS #>= S,
    NS #=< S + RemMax,
    gather_resources(B, R, MaxNR),
    NR.ore #=< MaxNR.ore,
    NR.clay #=< MaxNR.clay,
    NR.obsidian #=< MaxNR.obsidian,
    NB.ore #=< B.ore + 1,
    NB.clay #=< B.clay + 1,
    NB.obsidian #=< B.obsidian + 1.


clpfd:run_propagator(follow_move(Bp, Rem, M, R, B, S, NR, NB, NS), MState) :-
    (   M == 4 -> clpfd:kill(MState), do_move(Bp, Rem, geode, R, B, S, NR, NB, NS)
    ;   M == 1 -> clpfd:kill(MState), do_move(Bp, Rem, ore, R, B, S, NR, NB, NS)
    ;   M == 2 -> clpfd:kill(MState), do_move(Bp, Rem, clay, R, B, S, NR, NB, NS)
    ;   M == 3 -> clpfd:kill(MState), do_move(Bp, Rem, obsidian, R, B, S, NR, NB, NS)
    ;   M == 0 -> clpfd:kill(MState), do_move(Bp, Rem, nil, R, B, S, NR, NB, NS)
    ;   move_transition_contraints(Rem, R, B, S, NR, NB, NS)
    ).

build_delay(Bp, M1, M2, Res) :-
    clpfd:make_propagator(build_delay(Bp, M1, M2, Res), Prop),
    clpfd:init_propagator(M1, Prop),
    clpfd:init_propagator(M2, Prop),
    clpfd:init_propagator(Res, Prop),
    clpfd:trigger_once(Prop).


clpfd:run_propagator(build_delay(Bp, M1, M2, Res), MState) :-
    ((\+ var(M2)) -> clpfd:kill(MState)
    ; res_instantiated(Res) ->
        ((M1 == 0, buy_bot(Bp, ore, Res, _)) -> M2 #\= 1; true),
        ((M1 == 0, buy_bot(Bp, clay, Res, _)) -> M2 #\= 2; true),
        ((M1 == 0, buy_bot(Bp, obsidian, Res, _)) -> M2 #\= 3; true),
        ((M1 == 0, buy_bot(Bp, geode, Res, _)) -> M2 #\= 4; true)
    ; true).

force_geode(Bp, M, Res) :-
    clpfd:make_propagator(force_geode(Bp, M, Res), Prop),
    clpfd:init_propagator(M, Prop),
    clpfd:init_propagator(Res, Prop),
    clpfd:trigger_once(Prop).

clpfd:run_propagator(force_geode(Bp, M, Res), MState) :-
    ( (\+ var(M)) -> clpfd:kill(MState)
    ; (res_instantiated(Res), buy_bot(Bp, geode, Res, _)) -> clpfd:kill(MState), M #= 4
    ; true).

force_obsidian(Bp, M, Res) :-
    clpfd:make_propagator(force_obsidian(Bp, M, Res), Prop),
    clpfd:init_propagator(M, Prop),
    clpfd:init_propagator(Res, Prop),
    clpfd:trigger_once(Prop).

clpfd:run_propagator(force_obsidian(Bp, M, Res), MState) :-
    ( (\+ var(M)) -> clpfd:kill(MState)
    ; (res_instantiated(Res), \+ buy_bot(Bp, geode, Res, _), buy_bot(Bp, obsidian, Res, _)) -> clpfd:kill(MState), M #= 3
    ; true).


cap_build(MaxCost, M, Bots) :-
    clpfd:make_propagator(cap_build(MaxCost, M, Bots), Prop),
    clpfd:init_propagator(M, Prop),
    clpfd:init_propagator(Bots, Prop),
    clpfd:trigger_once(Prop).

clpfd:run_propagator(cap_build(MaxCost, M, Bots), MState) :-
    ( (\+ var(M)) -> clpfd:kill(MState)
    ; (bots_instantiated(Bots) -> 
        (MaxCost.ore =< Bots.ore -> M #\= 1)
        , (MaxCost.clay =< Bots.clay -> M #\= 2)
        , (MaxCost.obsidian =< Bots.obsidian -> M #\=3)
        )
    ; true).


model_moves(_, [_], [_], [_], [_]).
model_moves(Bp, [M1,M2|Ms], [R1, R2| Rs], [B1, B2 | Bs], [G1, G2| Gs]) :-
    length([M2|Ms], Rem),
    follow_move(Bp, Rem, M1, R1, B1, G1, R2, B2, G2),
    build_delay(Bp, M1, M2, R1),
    force_geode(Bp, M2, R2),
    force_obsidian(Bp, M2, R2),
    bp_maxcost(Bp, MaxCost),
    cap_build(MaxCost, M2, B2),
    model_moves(Bp, [M2|Ms], [R2 | Rs], [B2 | Bs], [G2 | Gs]).


model_resources(_, []).
model_resources(Max, [R|Rs]) :-
    R = resources{ore: O, clay: C, obsidian: Ob},
    O in 0..Max, C in 0..Max, Ob in 0..Max,
    model_resources(Max, Rs).

model_bots(_, []).
model_bots(L, [B|Bs]) :-
    B=robots{ore: O, clay: C, obsidian: Ob},
    O in 0..L, C in 0..L, Ob in 0..L,
    model_bots(L, Bs).

model(B, L, Ms, Rs, Bs, Gs) :-
    length(Ms, L),
    length(Rs, L),
    length(Bs, L),
    length(Gs, L),
    triangle(L, Max),
    Ms ins 0..4,
    Gs ins 0..Max,
    make_resources(resources, 0,0,0, IR),
    make_resources(robots, 1,0,0, IB),
    Rs = [IR|_],
    Bs = [IB|_],
    Gs = [0|_],
    model_resources(Max, Rs),
    model_bots(L, Bs),
    model_moves(B, Ms, Rs, Bs, Gs).


search_target(_Bp, _, [_M], [_R], [_B], [_G2]).
search_target(Bp, M1, [M1,M2|Ms], [R1, R2|Rs], [B1, B2|Bs], [G1, G2|Gs]) :- 
    length([M2|Ms], Rem),
    move_num(Move, M1),
    do_move(Bp, Rem, Move, R1, B1, G1, R2, B2, G2),
    !, search(Bp, [M2|Ms], [R2|Rs], [B2|Bs], [G2|Gs]).
search_target(Bp, M, [0,M2|Ms], [R1,R2|Rs], [B1, B2|Bs], [G1, G2|Gs]) :- 
    length([M2|Ms], Rem),
    do_move(Bp, Rem, nil, R1, B1, G1, R2, B2, G2),
    search_target(Bp, M, [M2|Ms], [R2|Rs], [B2|Bs], [G2|Gs]).

search(Bp, Ms, Rs, Bs, Gs) :-
    member(Move, [0,1,2,3,4]),
    search_target(Bp, Move, Ms, Rs, Bs, Gs).

smodel(L, Ms, Rs, Bs, Gs) :-
    length(Ms, L),
    length(Rs, L),
    length(Bs, L),
    length(Gs, L),
    make_resources(resources, 0,0,0, IR),
    make_resources(robots, 1,0,0, IB),
    Rs = [IR|_],
    Bs = [IB|_],
    Gs = [0|_].

max_geodes_search(B, Days, GeodeCount) :-
    model(B, Days, Ms, Rs, Bs, Gs),
    foreach((search(B, Ms, Rs, Bs, Gs), last(Gs, Score)), Max #>= Score),
    fd_inf(Max, GeodeCount).

max_geodes(B, Days, Ms, GeodeCount) :-
    length(Ms, Days),
    make_resources(resources, 0, 0, 0, IR),
    make_resources(robots, 1, 0, 0, IB),

    model(B, Days, Ms, [IR|_], [IB|_], [0|Gs]),
    model_vars(Ms, _, _, Gs, _),
    last(Gs, GeodeCount),
    once(labeling([leftmost, up, enum, max(GeodeCount)], Ms)),
    writeln(Ms).

good_moves([0,0,2,0,2,0,2,0,0,0,3,2,0,0,3,0,0,4,0,0,4,0,0,0]).

solve(Bs) :-
    max_geodes(Bs.1, 24, Geode),
    writeln(Geode),
    label([Geode]),
    writeln(Geode).

main([Inp]) :-
    read_program(Inp, Bs),
    writeln(Bs),
    test_geodes(Bs.1, Geodes),
    writeln(Geodes).