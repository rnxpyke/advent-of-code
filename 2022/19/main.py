import sys
import re

def memoize(f):
    memo = {}
    def helper(*args):
        if args not in memo: memo[args] = f(*args)
        return memo[args]
    return helper

def costs_dict(ore, clay, obsidian): return { "ore": ore, "clay": clay, "obsidian": obsidian }
def costs_tuple(ore, clay, obsidian): return (ore, clay, obsidian)
def costs(*args): return costs_tuple(*args)
reorder = ("ore", "clay", "obsidian", "geode")

def max_costs(bots):
    return { key: max(v[i] for v in bots.values()) for i,key in enumerate(reorder[:3]) }

def build_blueprint(ore_cost, clay_cost, obsidian_cost1, obsidian_cost2, geode_cost1, geode_cost_2):
    robots = {
        "ore": costs(ore_cost, 0, 0),
        "clay": costs(clay_cost, 0, 0),
        "obsidian": costs(obsidian_cost1, obsidian_cost2, 0),
        "geode": costs(geode_cost1, 0, geode_cost_2),
    }
    return { "robots": robots, "max_costs": max_costs(robots) }


def is_possible(bp, res, ro, move):
    return all(res >= cost for res, cost in zip(res[:3], bp["robots"][move]))


def possible_moves_time(bpi, res, ro):
    bp = blueprints[bpi]
    (ore_bots, clay_bots, obs_bots, geode_bots) = ro
    bots = { "ore": ore_bots, "clay": clay_bots, "obsidian": obs_bots, "geode": geode_bots}
    if is_possible(bp, res, ro, 'geode'):
        yield (0, 'geode')
        return
    if is_possible(bp, res, ro, 'obsidian'):
        yield (0, 'obsidian')
        return
    for m in reorder:
        if m != "geode" and bots[m] >= bp["max_costs"][m]: continue
        (ore_cost, clay_cost, obs_cost) = bp["robots"][m]
        if clay_cost != 0 and clay_bots == 0: continue
        if obs_cost != 0 and obs_bots == 0: continue
        tres, tro, time = res, ro, 0
        while not is_possible(bp, tres, tro, m):
            time += 1
            _, tres, tro = make_move_state(None, bpi, 100, tres, tro)
        yield time, m


def gather_resources(bp, resources, robots, tr):
    return tuple(cnt + robs for cnt, robs in zip(resources, robots))

def add_robot(res, robots):
    idx = list(reorder).index(res)
    return tuple(r + (1 if i == idx else 0) for i,r in enumerate(robots))

def consume_robot_resources(bp, res, ro):
    ore, clay, obsidian = tuple(res - cost for res,cost in zip(res[:3], bp["robots"][ro]))
    return (ore, clay, obsidian, res[-1])

def make_move_state(m, bpi, tr, res, ro):
    bp = blueprints[bpi]
    nres = consume_robot_resources(bp, res, m) if m is not None else res
    nro = add_robot(m, ro) if m is not None else ro
    cres = gather_resources(bp, nres, ro, tr)
    return (tr-1, cres, nro)

def make_move_time(t, m, bpi, tr, res, ro):
    for _ in range(t):
        tr, res, ro = make_move_state(None, bpi, tr, res, ro)
        if tr <= 0: return res[-1]
    return geode_count(bpi, *make_move_state(m, bpi, tr, res, ro))

def make_move(*args): return make_move_time(0, *args)

@memoize
def geode_count(bpi, tr, res, ro):
    bp = blueprints[bpi]
    if tr <= 0: return res[-1]
    return max(make_move_time(t, m, bpi, tr, res, ro) for t, m in possible_moves_time(bpi, res, ro))

def simulate(bpi, time): return geode_count(bpi, time, (0,0,0,0), (1,0,0,0))
def quality_level(bpi): return bpi * simulate(bpi, 24)

if __name__ == '__main__':
    inp = open(sys.argv[1]).read().strip()
    nums = [tuple(int(x) for x in re.findall('\d+', line)) for line in inp.split('\n')]
    blueprints = { args[0]: build_blueprint(*args[1:]) for args in nums }
    print(sum(quality_level(key) for key in blueprints))

    g1, g2, g3 = tuple(simulate(i, 32) for i in [1,2,3])
    print(g1 * g2 * g3)