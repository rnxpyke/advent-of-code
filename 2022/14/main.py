import sys
import numpy as np
from collections import defaultdict

class keydefaultdict(defaultdict):
    def __missing__(self, key):
        if self.default_factory is None:
            raise KeyError( key )
        else:
            ret = self[key] = self.default_factory(key)
            return ret

def sign(n):
    if n > 0: return 1
    if n < 0: return -1
    return 0

def draw_path(grid, last, point):
    lx, ly = last
    x, y = point
    while lx != x or ly != y:
        grid[(lx, ly)] = '#'
        lx += sign(x - lx)
        ly += sign(y - ly)
    grid[(x,y)] = '#'

def print_grid(grid):
    lx = min(x for x,y in grid.keys())
    rx = max(x for x,y in grid.keys())
    sy = max(y for x,y in grid.keys())
    for y in range(sy + 1):
        print("".join([grid[(x,y)] for x in range(lx, rx + 1)]))


def simulate_single(grid, spawn, cutoff):
    cx, cy = spawn
    if grid[spawn] != '.': return None
    while True:
        if cy >= cutoff: return None
        if grid[cx, cy+1] == '.':
            cy += 1
            continue
        if grid[cx-1, cy+1] == '.':
            cx -= 1
            cy += 1
            continue
        if grid[cx+1, cy+1] == '.':
            cx += 1
            cy += 1
            continue
        return cx, cy

def solve(paths, cutoff):
    sy = max(y for path in paths for x,y in path)
    grid = keydefaultdict(lambda key: '.' if key[1] != sy + 2 else '#')
    for path in paths:
        lx,ly = path[0]
        for x,y in path[1:]:
            draw_path(grid, (lx, ly), (x, y))
            lx, ly = x, y

    cnt = 0
    while True:
        res = simulate_single(grid, spawn=(500,0), cutoff=cutoff)
        if res is None: break
        grid[res] = 'o'
        cnt += 1
        # print_grid(grid)
    print(cnt)

if __name__ == '__main__':
    inp = open(sys.argv[1]).read().strip().split('\n')
    paths = [[tuple(int(v) for v in p.split(',')) for p in line.split(' -> ')] for line in inp]
    sy = max(y for path in paths for x,y in path)
    solve(paths, cutoff=sy+1)
    solve(paths, cutoff=sy+3)

