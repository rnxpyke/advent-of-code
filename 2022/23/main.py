import sys

def add_point(p, d):
    x,y = p
    dx,dy = d
    return (x+dx,y+dy)
N, E, S, W = [(0,-1), (1,0), (0,1), (-1,0)]
NE, NW = add_point(N,E), add_point(N,W)
SE, SW = add_point(S,E), add_point(S,W)


def prop_helper(elves, pos, checks):
    check = [add_point(pos, d) not in elves for d in checks]
    return all(check)

def propose(elves, pos, r):
    if prop_helper(elves, pos, [N,S,E,W,NE,SE,NW,SW]): return None
    order = [
        ([N,NE,NW], N),
        ([S,SE,SW], S),
        ([W,NW,SW], W),
        ([E,NE,SE], E),
    ]
    for i in range(4):
        checks, target = order[(i+r) % len(order)]
        if prop_helper(elves, pos, checks): return target
    return None

def simulate_step(elves, r):
    prop = {}
    for pos in elves:
        direction = propose(elves, pos, r)
        if direction is None: continue
        #print('i wanna move', pos, direction)
        target = add_point(pos, direction)
        if target not in prop: prop[target] = []
        prop[target].append(pos)
        elves[pos] = target
    for key, value in prop.items():
        if len(value) <= 1: continue
        for pos in value: elves[pos] = None
    res = {}
    finish = True
    for key, value in elves.items():
        if value is None: res[key] = None
        else: 
            res[value] = None
            finish = False
    return res, finish

def showboard(elves):
    start = -3
    size = 14
    board = [['.' for _ in range(start, start+size)] for _ in range(start, start+size)]
    for (x,y) in elves.keys():
        if x not in range(start, start+size): continue
        if y not in range(start, start+size): continue
        board[y - start][x - start] = '#'
    print("\n".join("".join(row) for row in board))

def simulate(elves, steps):
    for i in range(steps):
        #print('\n\n', i, '\n')
        elves, finish = simulate_step(elves, i)
        #showboard(elves)
    return elves

def simulate_fix(elves):
    finish = False
    r = 0
    while not finish:
        elves, finish = simulate_step(elves, r)
        r += 1
    return r

if __name__ == '__main__':
    inp = open(sys.argv[1]).read().strip().split('\n')
    elves = {}
    for y, line in enumerate(inp):
        for x, c in enumerate(line):
            if c == '#': elves[(x,y)] = None
    part1 = simulate(elves, 10)
    w = max(x for x,y in part1) - min(x for x,y in part1) + 1
    h = max(y for x,y in part1) - min(y for x,y in part1) + 1
    print(w*h - len(part1))

    print(simulate_fix(elves))
