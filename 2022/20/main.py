import sys

def to_list(cypher): return [n for _,n in cypher]

def mix(cypher, order):
    for elem in order:
        idx = cypher.index(elem)
        i,n = cypher.pop(idx)
        new_idx = (idx + n) % len(cypher)
        if new_idx == 0: new_idx = len(cypher)
        cypher.insert(new_idx, elem)

def check(cypher):
    nums_list = to_list(cypher)
    zero = nums_list.index(0)
    vs = [nums_list[(zero + idx) % len(nums_list)] for idx in [1000, 2000, 3000]]
    return sum(vs)

def part1(inp):
    nums = list(v for v in inp)
    mix(nums, inp)
    return check(nums)

def part2(inp):
    nums = list(v for v in inp)
    for _ in range(10):
        mix(nums, inp)
    return check(nums)


if __name__ == '__main__':
    bare_inp = [int(v) for v in open(sys.argv[1]).read().strip().split('\n')]
    inp = list(enumerate(bare_inp))
    print(part1(inp))

    key = 811589153
    inp2 = list((i, v*key) for i,v in enumerate(bare_inp))
    print(part2(inp2))