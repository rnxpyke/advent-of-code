import sys
import numpy as np

def parse_instr(parts):
    if parts[0] == 'addx': return ['addx', int(parts[1])]
    return ['noop']

def sim(instr, start=1):
    x = start
    for i in instr:
        if i[0] == 'noop': yield x
        if i[0] == 'addx':
            arg = i[1]
            yield x
            x += arg
            yield x 

if __name__ == '__main__':
    inp = open(sys.argv[1]).read().strip().split('\n')
    inst = [parse_instr(l.split()) for l in inp]

    trace = list(sim(inst))
    print(sum(trace[t-2] * t for t in range(20, 221, 40)))
    grid = np.zeros((6,40), dtype=int)

    for cycle, mid in enumerate([1] + trace):
        row = cycle // grid.shape[1]
        pos = cycle % grid.shape[1]
        if mid in [pos-1, pos, pos+1]:
            grid[row, pos] = 1

    for x in range(grid.shape[0]):
        print("".join("#" if e else "." for e in grid[x,:] != 0))
