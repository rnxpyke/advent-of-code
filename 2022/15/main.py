import sys
import re

def dist(a,b):
    ax,ay = a
    bx,by = b
    return abs(ax-bx)+abs(ay-by)

def dist_cover(sens, beac): return dist(sens, beac)

def covered(sens, d, point):
    return dist(sens, point) <= d

def beacrange(sens, d, yl):
    sx, sy = sens
    dtoy = abs(yl - sy)
    if dtoy >= d: return None 
    lx = sx - (d - dtoy)
    rx = sx + (d - dtoy)
    assert(covered(sens, d, (lx, yl)))
    assert(covered(sens, d, (rx, yl)))
    assert(not covered(sens, d, (lx-1, yl)))
    assert(not covered(sens, d, (rx+1, yl)))
    return (lx, rx)

def overlaps(me, other):
    ss, se = me
    os, oe = other
    return se >= os and ss <= oe

if __name__ == '__main__':
    inp = open(sys.argv[1]).read().strip()
    coords = sorted([((int(sx), int(sy)), (int(bx),int(by))) for sx,sy,bx,by in re.findall('Sensor at x=(-?\d+), y=(-?\d+): closest beacon is at x=(-?\d+), y=(-?\d+)', inp)])
    YLINE = 2000000
    ranges = []
    for sens, beac in coords:
        res = beacrange(sens, dist(sens, beac), YLINE)
        if res is not None:
            for i in range(len(ranges)):
                if overlaps(ranges[i], res):
                    ranges[i] = (min(ranges[i][0], res[0]), max(ranges[i][1], res[1]))
                    break
            else:
                ranges.append(res)
    print(sum(r[1] - r[0] for r in ranges))

    for y in range(4000001):
        x = 0
        for sens, beac in coords:
            r = beacrange(sens, dist(sens, beac), y)
            if r is not None:
                lx, rx = r
                if lx <= x <= rx:
                    x = rx + 1
        if x < 4000001:
            print(x * 4000000 + y)