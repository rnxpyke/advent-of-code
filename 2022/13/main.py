import sys
from functools import cmp_to_key
def sign(n):
    if n > 0: return 1
    if n < 0: return -1
    return 0

def ordered(left, right):
    match (left, right):
        case (int(_), int(_)): return sign(right - left)
        case (int(_), list(_)): return ordered([left], right)
        case (list(_), int(_)): return ordered(left, [right])
        case (list(_), list(_)):
            for nl, nr in zip(left, right):
                res = ordered(nl, nr)
                if res != 0: return res
            return sign(len(right) - len(left))
    raise "No match"

if __name__ == '__main__':
    inp = open(sys.argv[1]).read().strip().split('\n\n')
    pairs = [tuple(eval(side) for side in pair.split('\n')) for pair in inp]
    print(sum(idx+1 for idx, (left, right) in enumerate(pairs) if ordered(left, right) == 1))
    dec1 = [[2]]
    dec2 = [[6]]
    values = sorted([e for p in pairs for e in p] + [dec1, dec2], key=cmp_to_key(ordered), reverse=True)
    #for v in values: print(v)
    idx1 = values.index(dec1) + 1
    idx2 = values.index(dec2) + 1
    print(idx1 * idx2)