import sys

if __name__ == '__main__':
    inp = open(sys.argv[1]).read().strip().split()
    line = inp[0]

    for i in range(1, len(line)):
        test = line[max(0,i-4):i]
        if len(set(test)) == 4:
            print(i)
            break

    for i in range(1, len(line)):
        test = line[max(0,i-14):i]
        if len(set(test)) == 14:
            print(i)
            break