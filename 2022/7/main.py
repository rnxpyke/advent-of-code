import sys
import re
from itertools import groupby
from anytree import Node, RenderTree, Resolver, AsciiStyle, PreOrderIter, PostOrderIter

from dataclasses import dataclass

@dataclass
class Cmd:
    cmd: any
    arg: any
    out: any

def label_inp(inp):
    cmd = 0
    for line in inp:
        if line[0] == '$': cmd += 1
        yield (cmd, line)

def parse_input(inp):
    inp = inp.split('\n')
    res = [list(l for j,l in v) for i,v in groupby(list(label_inp(inp)), lambda x: x[0])]
    cmds = []
    for group in res:
        head, *out = group
        _, cmd, *arg = head.split()
        arg = arg[0] if len(arg) else None
        out = [l.split() for l in out]
        out = [(name, None if a == 'dir' else int(a)) for a, name in out]
        cmds.append(Cmd(cmd, arg, out))
    return cmds

def build_fs(cmds):
    root = Node('root', file=False, size=None)
    cur = root

    r = Resolver('name')
    for cmd in cmds:
        print(cmd)
        if cmd.cmd == 'cd':
            if cmd.arg == '/': cur = r.get(cur, '/root')
            else: cur = r.get(cur, cmd.arg)
        if cmd.cmd == 'ls':
            for name, size in cmd.out:
                Node(name, parent=cur, size=size, file=size!=None)
    return root

def measure_dir_sizes(fs):
    for node in PostOrderIter(fs):
        if node.size == None: node.size = sum(child.size for child in node.children)

if __name__ == '__main__':
    cmds = parse_input(open(sys.argv[1]).read().strip())
    fs = build_fs(cmds)
    #print(RenderTree(fs, style=AsciiStyle).by_attr())
    measure_dir_sizes(fs)
    
    part1 = sum(n.size for n in PreOrderIter(fs) if n.file == False and n.size <= 100000)
    print(part1)
    
    remaining = 70000000 - fs.size
    gc = 30000000 - remaining
    part2 = min(n.size for n in PreOrderIter(fs) if n.file == False and n.size >= gc)
    print(part2)