#!/usr/bin/env swipl

:- initialization(main, main).
:- use_module(library(clpfd)).

read_snafus(Name, Snafus) :-
    open(Name, read, File),
    read_string(File, _, S),
    split_string(S, "\n", " ", Snafus).

place_value('=', -2).
place_value('-', -1).
place_value('0', 0).
place_value('1', 1).
place_value('2', 2).
snafu_vlist(Snafu, Vs) :- maplist(place_value, Snafu, Vs).

rev_plist([], _).
rev_plist([H|T], H) :-
    Acc #= H*5,
    rev_plist(T, Acc).

values_places(Vs, Ps) :-
    length(Vs, L),
    length(Ps, L),
    Vs ins -2..2,
    reverse(Ps, RPs),
    rev_plist(RPs, 1).

mul(A,B,R) :- R #= A*B.
vlist_num(Vs, Num) :-
    values_places(Vs, Ps),
    maplist(mul, Vs, Ps, Factors),
    sum(Factors, #=, Num),
    once(label(Vs)), !.

snafu_num(Snafu, Num) :- \+var(Snafu), !, string_chars(Snafu, Cs), snafu_vlist(Cs, Vs), vlist_num(Vs, Num).
snafu_num(Snafu, Num) :- \+var(Num), !, vlist_num(Vs, Num), snafu_vlist(Cs, Vs), string_chars(Snafu, Cs).

main([Inp]) :-
    read_snafus(Inp, Snafus),
    maplist(snafu_num, Snafus, Nums),
    sum(Nums, #=, Res),
    snafu_num(SnafuTotal, Res),
    writeln(SnafuTotal).