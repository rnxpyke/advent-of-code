import sys

values = { '0': 0, '1': 1, '2': 2, '-': -1, '=': -2}

def snafu_to_num(snafu):
    return sum(5**i * values[c] for i,c in enumerate(reversed(snafu)))

def num_to_snafu(dec):
    return None

test_table = [
    (1, '1'),
    (2, '2'),
    (3, '1='),
    (4, '1-'),
    (5, '10'),
    (6, '11'),
    (7, '12'),
    (8, '2='),
    (9, '2-'),
    (10, '20'),
    (15, '1=0'),
    (20, '1-0'),
    (2022, '1=11-2'),
    (12345, '1-0---0'),
    (314159265, '1121-1110-1=0'),
]

if __name__ == '__main__':
    snafus = open(sys.argv[1]).read().strip().split('\n')
    for dec, snafu in test_table:
        assert snafu_to_num(snafu) == dec, f"exected '{snafu}' to be {dec}, got {snafu_to_num(snafu)}"
        assert num_to_snafu(dec) == snafu, f"expected {dec} to be '{snafu}', got '{num_to_snafu(dec)}'"
    total = sum(snafu_to_num(snafu) for snafu in snafus)
    print(total)
    print(num_to_snafu(total))