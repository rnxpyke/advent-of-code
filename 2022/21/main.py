import sys

def op(l):
    name, rest = l.split(': ')
    try:
        const = int(rest)
        return (name, 'imm', const)
    except:
        a1, op, a2 = rest.split(' ')
        return (name, op, (a1, a2))

def say(name):
    op, args = monkeys[name]
    if op == 'imm': return args
    if op == '+': return say(args[0]) + say(args[1])
    if op == '-': return say(args[0]) - say(args[1])
    if op == '*': return say(args[0]) * say(args[1])
    if op == '/': return say(args[0]) // say(args[1])

if __name__ == '__main__':
    inp = open(sys.argv[1]).read().strip().split('\n')
    monkeys = { key: (op, v) for key, op, v in [op(l) for l in inp] }
    print(say('root'))