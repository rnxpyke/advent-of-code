#!/usr/bin/env swipl

:- initialization(main, main).
:- use_module(library(clpfd)).

string_atom(S, A) :- atom_string(A,S).
parse_args(Args, Value) :- number_string(Value, Args), !.
parse_args(Args, Value) :-
    split_string(Args, " ", "", StrParts),
    maplist(string_atom, StrParts, [L,O,R]),
    Value =.. [O,L,R].

parse_opt(Str, Opt) :-
    split_string(Str, ":", " ", [NameS, Args]),
    atom_string(Name, NameS),
    parse_args(Args, Value),
    Opt = (Name=Value).

noteof(L) :- \+ L = end_of_file.
read_program(P, F) :-
    open(F, read, File),
    read_string(File, _, S),
    split_string(S, "\n", "", Lines),
    maplist(parse_opt, Lines, Instrs),
    dict_create(P, monkeys, Instrs),
    close(File), !.


operation(+, L, R, Res) :- Res #= L + R.
operation(-, L, R, Res) :- Res #= L - R.
operation(/, L, R, Res) :- Res #= L // R.
operation(*, L, R, Res) :- Res #= L * R.
operation(=, L, R, Res) :- L #= R, Res = true.

say_human(P, H) :-
    P.root =.. [_, A, B],
    say(P, H, A, Left),
    say(P, H, B, Right),
    Left #= Right.
say_test(P,H, L,R) :-
    P.root =..[_, A, B],
    say(P,H,A,L),
    say(P,H,B,R).

say(_, H, humn, Res) :- Res #= H, !.

say(P, _, Monkey, Res) :-
    T = P.Monkey,
    integer(T), !,
    Res = T.

say(P, H, Monkey, Res) :-
    P.Monkey =.. [Op, A, B], !,
    say(P, H, A, Left),
    say(P, H, B, Right),
    operation(Op, Left, Right, Res).

main([Inp|_]) :-
    read_program(P, Inp),
    say(P, P.humn, root, Res),
    write(Res), write('\n'), !,
    say_human(P, H),
    label([H]),
    write(H),
    write('\n').