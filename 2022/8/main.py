import numpy as np
import sys

def part1(grid):
    grid = np.copy(grid)
    # add one to all threes because border trees with height 0 mess up the alg
    grid += 1
    top, bot, left, right = [np.zeros(grid.shape, dtype=int) for _ in range(4)]
    for i in range(1, grid.shape[0]):  top[i,:] = np.max((grid[i-1,:], top[i-1,:]), axis=0)
    for i in range(1, grid.shape[1]): left[:,i] = np.max((grid[:,i-1],left[:,i-1]), axis=0)
    for i in reversed(range(grid.shape[0] - 1)):   bot[i,:] = np.max((grid[i+1,:],  bot[i+1,:]), axis=0)
    for i in reversed(range(grid.shape[1] - 1)): right[:,i] = np.max((grid[:,i+1],right[:,i+1]), axis=0)
    threshold = np.min((top,bot,left,right), axis=0)
    is_vis = threshold < grid
    return is_vis.sum()

def score(grid, y, x):
    res = 1
    height = grid[y,x]
    for line in [grid[y, x+1:], grid[y+1:, x], np.flip(grid[y, 0:x]), np.flip(grid[0:y, x])]:
        for i,e in enumerate(line):
            if e >= height:
                res *= (i + 1)
                break
        else: res *= len(line)
    return res

def part2(grid):
    res = 0
    for i in range(grid.shape[0]):
        for j in range(grid.shape[1]):
            cur = score(grid, i, j)
            if res < cur: res = cur
    return res

if __name__ == '__main__':
    inp = open(sys.argv[1]).read().strip().split('\n')
    grid = np.array([[int(c) for c in line] for line in inp])
    print(part1(grid))
    print(part2(grid))
