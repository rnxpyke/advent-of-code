import sys
import re

pat = 'Valve (\w+) has flow rate=(\d+); tunnels? leads? to valves? (\w+(?:, \w+)*)'

def bits(n):
    while n:
        b = n & (~n+1)
        yield b
        n ^= b

def bitset_test(bs, v):
    return (bs & (1 << valv_idx.index(v))) != 0

def bitset_set(bs, v):
    return (bs | (1 << valv_idx.index(v)))

def bitset_score(bs):
    return sum(valves[valv_idx[b.bit_length()-1]][0] for b in bits(bs))

def find_dists(cur, goal):
    dist = {cur: (0, cur)}
    to_visit = [cur]
    for node in to_visit:
        for n in valves[node][1]:
            if n not in dist: 
                dist[n] = (dist[node][0] + 1, node)
                to_visit.append(n)
            else:
                if dist[n][0] > dist[node][0] + 1:
                    dist[n] = (dist[node] + 1, node)
    return dist

def memoize(f):
    memo = {}
    def helper(*args):
        if args not in memo:            
            memo[args] = f(*args)
            # print("memo insert", args, memo[args])
        return memo[args]
    return helper


def gen_moves(opened, current, time_remaining):
    for valve in interesting.keys():
        if not bitset_test(opened, valve):
            time = shortest_paths[current][valve][0]
            assert time > 0
            if time <= time_remaining:
                yield valve, time

@memoize
def path_score(opened, current, time_remaining):
    if time_remaining <= 0: return 0
    if len(list(bits(opened))) >= val_count: return time_remaining * bitset_score(opened)
    if not bitset_test(opened, current) and valves[current][0] > 0:
        new_opened = bitset_set(opened, current)
        return bitset_score(opened) + path_score(new_opened, current, time_remaining - 1)
    res = time_remaining * bitset_score(opened)
    for target, t in gen_moves(opened, current, time_remaining):
        score = t * bitset_score(opened) + path_score(opened, target, time_remaining - t)
        if score > res: res = score
    return res

def gen_considers(low, high):
    cnt = len(interesting)
    for i in range(2**cnt):
        opened, complement = 0, 0
        for b in range(cnt):
            valve = interesting_idx[b]
            if (i & (1 << b)) != 0: opened = bitset_set(opened, valve)
            else: complement = bitset_set(complement, valve)
        vcnt = len(list(bits(opened)))
        if cnt//2 - low > vcnt: continue
        if cnt//2 + high < vcnt: continue
        yield opened, complementtqdm

def path_score_2(tr, start, opened_me, opened_elephant):
    score_me = path_score(opened_me, start, tr) - bitset_score(opened_me) * tr
    score_ele = path_score(opened_elephant, start, tr) - bitset_score(opened_elephant) * tr
    return score_me + score_ele

if __name__ == '__main__':
    inp = open(sys.argv[1]).read().strip()
    valves = { key: (int(flow), ns.split(', ')) for key, flow, ns in re.findall(pat, inp) }
    valv_idx = [key for key in valves.keys()]
    interesting = { key: (flow, ns) for key, (flow, ns) in valves.items() if flow > 0 }
    interesting_idx = [key for key in interesting.keys()]
    val_count = len(interesting)

    shortest_paths = { source: find_dists(source, None) for source in valv_idx }
    
    print(path_score(0, 'AA', 30))
    print(max(path_score_2(26, 'AA', a,b) for a, b in gen_considers(0,1)))
 