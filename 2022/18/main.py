import sys

def neigbours(v):
    x,y,z = v
    for dx,dy,dz in [(1,0,0),(-1,0,0),(0,1,0),(0,-1,0),(0,0,1),(0,0,-1)]:
        yield x+dx,y+dy,z+dz

def within(start, end, point):
    sx,sy,sz = start
    ex,ey,ez = end
    x,y,z = point
    if not (sx <= x <= ex): return False
    if not (sy <= y <= ey): return False
    if not (sz <= z <= ez): return False
    return True

def bfs(start, bb):
    dist = { start: 0 }
    to_visit = [start]
    while to_visit != []:
        v = to_visit.pop(0)
        for n in neigbours(v):
            if n in voxels: continue
            if not within(start, bb, n): continue
            if n not in dist:
                dist[n] = dist[v] + 1
                to_visit.append(n)
    return dist

def bb_size(start, bb):
    sx,sy,sz = start
    ex,ey,ez = bb
    dx = ex-sx+1
    dy = ey-sy+1
    dz = ez-sz+1
    return dx*dy*dz

def part1():
    print(sum(n not in voxels for v in voxels for n in neigbours(v)))

def part2():
    bb_x = 3 + max(x for x,y,z in voxels)
    bb_y = 3 + max(y for x,y,z in voxels)
    bb_z = 3 + max(z for x,y,z in voxels)
    bb = (bb_x,bb_y,bb_z)
    start = (-3,-3,-3)
    exterior = bfs(start, bb)
    #print(exterior)
    for e in exterior:
        for n in neigbours(e):
            if within(start, bb, n):
                assert(bool(n in exterior) != bool(n in voxels))

    print(sum(bool(n in exterior) for v in voxels for n in neigbours(v)))
if __name__ == '__main__':
    lines = open(sys.argv[1]).read().strip().split('\n')
    inp = [tuple(int(x) for x in l.split(',')) for l in lines]
    voxels = set(inp)

    part1()
    part2()