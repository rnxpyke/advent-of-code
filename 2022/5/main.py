import sys
import re

from dataclasses import dataclass

@dataclass
class Move:
    count: int
    source: int
    target: int


def build_stack(stackstrs, value):
    items = [s[value] for s in stackstrs]
    return list(filter(lambda i: i != ' ', reversed(items)))


def part1(stacks, moves):
    for move in moves:
        for _ in range(move.count):
            item = stacks[move.source].pop()
            stacks[move.target].append(item)
    return "".join(stack[-1] for stack in stacks.values())

def part2(stacks, moves):
    for move in moves:
        items = []
        for _ in range(move.count):
            items.append(stacks[move.source].pop())
        for _ in range(move.count):
            stacks[move.target].append(items.pop())
    return "".join(stack[-1] for stack in stacks.values())


if __name__ == '__main__':
    inp = open(sys.argv[1]).read()
    stacks = re.match('(\s*\[.*\n)+.*', inp, re.MULTILINE).group(0)
    MOVE_PAT = 'move (\d+) from (\d+) to (\d+)'
    moves = [Move(int(a), int(b), int(c)) for a,b,c in re.findall(MOVE_PAT, inp)]
    *stackstrs, labels = stacks.split('\n')
    indexes = { int(label): labels.find(label) for label in re.findall('\d+', labels) }
    stacks = { key: build_stack(stackstrs, value) for key, value in indexes.items() }
    
    print(part1({ k: v.copy() for k,v in stacks.items()}, moves))
    print(part2({ k: v.copy() for k,v in stacks.items()}, moves))