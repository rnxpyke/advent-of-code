import sys

def memoize(f):
    memo = {}
    def helper(*args):
        if args not in memo: memo[args] = f(*args)
        return memo[args]
    return helper

table = [('>', (1, 0)), ('<', (-1,0)), ('^', (0, -1)), ('v', (0, 1))]
directions = { symbol: direction for symbol, direction in table}
symbols = {direction: symbol for symbol, direction in table }

def parse_input(inp):
    lines = [l for l in inp.split('\n')]
    height = len(lines)
    width = len(lines[0])
    blizards = {}
    for y,row in enumerate(lines):
        for x, c in enumerate(row):
            if c not in directions:
                if c == '#': blizards[(x,y)] = '#'
                continue
            if (x,y) not in blizards: blizards[(x,y)] = set()
            blizards[(x,y)].add(directions[c])
    return blizards, height, width

def show_board(blizards, pos=None):
    rows = [['.' for _ in range(width)] for _ in range(height)]
    for (x,y), value in blizards.items():
        if type(value) is set:
            if len(value) == 1: rows[y][x] = symbols[next(iter(value))]
            else: rows[y][x] = str(len(value))
            continue
        if value == '#': rows[y][x] = '#'
    if pos is not None:
        x,y = pos
        rows[y][x] = 'E'
    return "\n".join(["".join(row) for row in rows])

def add_point(p,d):
    x,y = p
    dx,dy = d
    return (x+dx,y+dy)

def step(blizards):
    new_blizards = {}
    for pos, value in blizards.items():
        if value == '#': new_blizards[pos] = '#'
        if type(value) is set:
            for d in value:
                npos = add_point(pos, d)
                if npos in blizards and blizards[npos] == '#':
                    npos = add_point(npos, d)
                    npos = add_point(npos, d)
                    x,y = npos
                    npos = (x % width, y % height)
                if npos not in new_blizards: new_blizards[npos] = set()
                new_blizards[npos].add(d)
    return new_blizards

@memoize
def board_at(time):
    if time <= 0: return {k:v for k,v in blizards.items()}
    return step(board_at(time - 1))

def moves(pos):
    yield pos
    for d in directions.values():
        nx, ny = add_point(pos, d)
        if not(0 <= nx < width): continue
        if not(0 <= ny < height): continue
        yield nx, ny

def possible_moves(x, y, time):
    for m in moves((x,y)):
        if m not in board_at(time+1): yield m[0], m[1], time+1


def bfs(spos, epos, time=0):
    sx, sy = spos
    start = (sx, sy, time)
    dist = { start: 0 }
    to_visit = [start]
    while to_visit != []:
        v = to_visit.pop(0)
        for n in possible_moves(*v):
            if n not in dist:
                dist[n] = dist[v] + 1
                to_visit.append(n)
            if epos[0] == n[0] and epos[1] == n[1]:
                return dist[n]
    return dist

if __name__ == '__main__':
    inp = open(sys.argv[1]).read().strip()
    blizards, height, width = parse_input(inp)
    board = {k:v for k,v in blizards.items()}
    end = (width-2, height-1)
    etime = bfs((1,0), end)
    print(etime)
    stime = bfs(end, (1,0), time=etime)
    rttime = bfs((1,0), end, time=etime+stime)
    print(etime + stime + rttime)
