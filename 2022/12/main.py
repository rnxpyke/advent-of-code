import sys
import numpy as np

def neighbours(shape, py, px):
    ns = [(py + y, px + x) for y,x in [(1,0), (0,1), (-1,0), (0, -1)]]
    return [(y,x) for y,x in ns if (0 <= y < shape[0]) and (0 <= x < shape[1])]


def find_dists(heights, start):
    dist = np.ones(heights.shape, np.int32) * 999
    sy, sx = start
    dist[sy, sx] = 0
    to_visit = [(sy, sx)]
    for py, px in to_visit:
        for y, x in neighbours(heights.shape, py, px):
            if heights[y,x] <= heights[py,px] + 1:
                if dist[y,x] > dist[py,px] + 1:
                    dist[y,x] = dist[py, px] + 1
                    to_visit.append((y,x))
    return dist

if __name__ == '__main__':
    inp = open(sys.argv[1]).read().strip().split()
    grid = np.array([[c for c in row] for row in inp])
    (sy, *_), (sx, *_) = np.where(grid == 'S')
    (gy, *_), (gx, *_) = np.where(grid == 'E')
    grid[sy,sx] = 'a'
    grid[gy,gx] = 'z'
    heights = grid.view(np.int32) - ord('a') + 1
    print(find_dists(heights, (sy, sx))[gy, gx])
    ys, xs = np.where(heights == 1)
    res = 999
    for y, x in zip(ys, xs):
        dist = find_dists(heights, (y,x))[gy,gx]
        if res > dist: res = dist
    print(res)
