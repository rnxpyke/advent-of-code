import numpy as np
import sys
from collections import Counter
import math

def sign(n):
    if n > 0: return 1
    if n < 0: return -1
    return 0

grid = [(i,j) for j in range(-1,2) for i in range(-1,2)]
dir_map = {'U': (0, 1), 'D': (0, -1), 'L': (-1, 0), 'R': (1,0)}

def touching(head, tail):
    hx, hy = head
    return tail in [(hx + x, hy + y) for x,y in grid]

def update_knot(head, tail):
    if touching(head, tail): return tail
    hx, hy = head
    tx, ty = tail
    return tx + sign(hx - tx), ty + sign(hy - ty)


def move_to(self, other):
    x,y = self
    dx, dy = other
    return (x + dx, y + dy)
        
def sim(moves, length):
    visited = Counter()
    rope = [(0,0) for _ in range(length)]
    visited.update([rope[-1]])
    for d, c in moves:
        for _ in range(c):
            rope[0] = move_to(rope[0], d)
            for k in range(1, length):
                rope[k] = update_knot(rope[k-1], rope[k])
            visited.update([rope[-1]])
    return len(visited)

if __name__ == '__main__':
    inp = open(sys.argv[1]).read().strip().split('\n')
    moves = [(dir_map[m.split()[0]], int(m.split()[1])) for m in inp]
    print(sim(moves, 2))
    print(sim(moves, 10))