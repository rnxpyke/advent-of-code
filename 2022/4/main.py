import sys
import re
from dataclasses import dataclass

@dataclass
class Elf:
    start: int
    end: int

    def contains(self, other):
        return self.start <= other.start and self.end >= other.end

    def overlaps(self, other):
        return self.end >= other.start and self.start <= other.end

def fully_contained(left, right):
    return left.contains(right) or right.contains(left)



if __name__ == '__main__':
    lines = [re.split('-|,', x) for x in open(sys.argv[1]).read().strip().split()]
    elfes = [(Elf(int(a), int(b)), Elf(int(c), int(d))) for a,b,c,d in lines]
    print(sum(fully_contained(left, right) for left, right in elfes))
    print(sum(left.overlaps(right) for left, right in elfes))