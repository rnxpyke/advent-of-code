import sys

def max_calories(input):
    return max(sum(inv) for inv in input)

def top_calories(input):
    return sorted([sum(inv) for inv in input])[-3:]

if __name__ == '__main__':
    inventories = open(sys.argv[1]).read().split('\n\n') 
    input = [list(map(int, inv.split('\n'))) for inv in inventories]
    
    print(max_calories(input))
    print(sum(top_calories(input)))
    
