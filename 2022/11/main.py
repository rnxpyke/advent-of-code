import sys
import re
import numpy as np
import math
from dataclasses import dataclass
from copy import deepcopy


patt = """Monkey (?P<id>.*):
\s*Starting items: (?P<items>\d+(?:, \d+)*)
\s*Operation: new = old (?P<op>\S*) (?P<arg>\d+|\S*)
\s*Test: divisible by (?P<divisor>\d+)
\s*If true: throw to monkey (?P<true_target>\d+)
\s*If false: throw to monkey (?P<false_target>\d+)"""

@dataclass
class Monkey:
    items: [int]
    op: any
    arg: str
    divisor: int
    true_target: int
    false_target: int
    inspections: int = 0

def make_monkey(m):
    items = [int(i) for i in m['items'].split(', ')]
    divisor = int(m['divisor'])
    true_target = int(m['true_target'])
    false_target = int(m['false_target'])
    arg = 'old' if m['arg'] == 'old' else int(m['arg'])
    op = None
    if m['op'] == '+': op = lambda x, y: x + y
    if m['op'] == '*': op = lambda x, y: x * y
    return Monkey(items, op, arg, divisor, true_target, false_target)

def update_item(divisors, op, item, argv):
    return { d: (op(item[d], argv[d])) % d for d in divisors }

def sim(monkeys, rounds, relief=True):
    divisors = [m.divisor for m in monkeys.values()]
    lcm = math.lcm(*divisors)
    for _ in range(rounds):
        for m in monkeys.values():
            for item in m.items:
                m.inspections += 1
                arg_v = item if m.arg == 'old' else m.arg
                item = m.op(item, arg_v)
                if relief: item //= 3
                item %= lcm
                if item % m.divisor == 0: monkeys[m.true_target].items.append(item)
                else: monkeys[m.false_target].items.append(item)
            m.items = []
    
    *_, second, first = sorted([m.inspections for m in monkeys.values()])
    return second * first

if __name__ == '__main__':
    inp = open(sys.argv[1]).read().strip()
    r = re.compile(patt)
    res = [m.groupdict() for m in r.finditer(inp)]
    monkeys = {int(m['id']): make_monkey(m) for m in res}

    print(sim(deepcopy(monkeys), 20))
    print(sim(deepcopy(monkeys), 10000, relief=False))