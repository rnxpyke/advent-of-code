import sys
import re
from tqdm import tqdm
import numpy as np
import math
from transforms3d.euler import euler2mat
from transforms3d.affines import compose

def sign(n):
    if n > 0: return 1
    if n < 0: return -1
    return 0

def try_parse_int(s, base=10, val=None):
  try: return int(s, base)
  except ValueError: return val


left, right, up, down = [(-1,0), (1,0), (0,-1), (0,1)]
rotate = {
    "L": { right:   up,   up: left, left: down, down: right },
    "R": { right: down, down: left, left:   up,   up: right }
}
facing_score = { right: 0, down: 1, left: 2, up: 3 }

def tp1(board, size, x,y, facing):
    sx, sy = size
    dx, dy = facing
    while True:
        x = (x + dx) % sx
        y = (y + dy) % sy
        if (x,y) in board: return x,y,facing

teleports = {}
def line(start, end):
    sx, sy = start
    ex, ey = end
    x, y = sx, sy
    while (x,y) != (ex, ey):
        assert x>=0, f"line {start} {end}"
        assert y>=0, f"line {start} {end}"
        yield (x,y)
        dx, dy = sign(ex - x), sign(ey - y)
        x += dx
        y += dy
    yield end

def inv(facing):
    x,y = facing
    return (x*-1, y*-1)

def add_line(instart, inend, infacing, outstart, outend, outfacing):
    for pin, pout in zip(line(instart, inend), line(outstart, outend)):
        teleports[(*pin, infacing)] = (*pout, outfacing)
        teleports[(*pout, inv(outfacing))] = (*pin, inv(infacing))

add_line((1,100), (50, 100), up, (50, 51),(50, 100), right) #left-top
add_line((101, 51), (101, 100), right, (101,51), (150,51), up) #top-right
add_line((51,151), (51,200), right, (51,151), (100,151), up) #down-front
add_line((101,101), (101,150), right, (151,50), (151,1), left) #front-right
add_line((0, 151), (0, 200), left, (51,0), (100,0), down) #down-back
add_line((1,201), (50,201), down, (101,0), (150,0), down) #down-right
add_line((0, 101), (0, 150), left, (50, 50), (50, 1), right) #left-back

def tp2(board, size, x,y, facing):
    if (x,y,facing) in teleports:
        tx, ty, facing = teleports[(x,y,facing)]
        dx, dy = facing
        nx = tx+dx
        ny = ty+dy
        #assert (nx,dy) in board, f"teleport not in board: {(nx,ny)}"
        return nx, ny, facing
    assert True == False, f"field not in teleport error {(x,y)}"

def move(board, size, pos, facing, tp):
    sx, sy = size
    x,y = pos
    dx,dy = facing
    nx = x + dx
    ny = y + dy
    if (nx,ny) not in board:
        nx,ny,facing = tp(board, size, nx,ny, facing)
    return (nx,ny),facing

def walk(tiles, pos, size, tp):
    facing = right
    for i,direction in enumerate(directions):
        if type(direction) is str: 
            facing = rotate[direction][facing]
            continue
        for _ in range(direction):
            n_pos,n_facing = move(tiles, size, pos, facing, tp)
            assert n_pos in tiles, f"error in iter {i}, {pos}, {n_pos}, {facing}"
            if tiles[n_pos] == '#': break
            pos = n_pos
            facing = n_facing
    #print(pos, facing)
    x,y = pos
    return 4*x + 1000 * y + facing_score[facing]

def part1(tiles, pos, size): return walk(tiles, pos, size, tp1)
def part2(tiles, pos, size): return walk(tiles, pos, size, tp2)

def build_map(lines):
    tiles = {}
    start = None
    for y, line in enumerate(lines, start=1):
        for x, c in enumerate(line, start=1):
            if start is None and c == '.': 
                start = (x,y)
            if c != ' ': tiles[(x,y)] = c
    sx = 1 + max(x for x,y in tiles)
    sy = 1 + max(y for x,y in tiles)
    size = (sx,sy)
    return tiles, start, size



if __name__ == '__main__':
    inp = open(sys.argv[1]).read().rstrip()
    *lines, _, directions = list(inp.split("\n"))
    directions = [try_parse_int(v, val=v) for v in re.findall('\d+|L|R', directions)]
    print(part1(*build_map(lines)))
    print(part2(*build_map(lines)))
    #zone_size = 4 if sys.argv[1] == 'example.txt' else 50

