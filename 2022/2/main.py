import sys
from enum import IntEnum

Var = IntEnum('Variant', ['R', 'P', 'S'])

def match(me, other):
    match (me, other):
        case (a, b) if a == b: return 0
        case (Var.R, Var.S) | (Var.S, Var.P) | (Var.P, Var.R): return 1
        case _: return -1

def score(me, other):
    return me + 3 * (match(me, other) + 1)

table = {'A': Var.R, 'B': Var.P, 'C': Var.S, 'X': Var.R, 'Y': Var.P, 'Z': Var.S }
table2 = {'X': -1, 'Y': 0, 'Z': 1}

def find_response(other, outcome):
    for x in [Var.R, Var.P, Var.S]:
        if match(x, other) == outcome: return x

if __name__ == '__main__':
    lines = open(sys.argv[1]).read().strip().split('\n')
    inp = [tuple(line.split()) for line in lines]
    decoded = [(table[other], table[me], table2[me]) for other, me in inp]
    print(sum(score(me, other) for (other,me, _) in decoded))

    guide2 = [(other, find_response(other, out)) for other, me, out in decoded]
    print(sum(score(me, other) for (other,me) in guide2))