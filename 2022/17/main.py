import sys
from copy import deepcopy
from tqdm import tqdm
import math

pole = {(0,0), (0, 1), (0,2), (0,3)}
plus = {(1,0), (0,1), (1,1), (2,1), (1,2)}
angle = {(0,0), (1,0), (2,0), (2,1), (2,2)}
flat = {(0,0), (1,0), (2, 0), (3, 0)}
square = {(0,0), (1,0), (0,1), (1,1)}
shapes = [flat, plus, angle, pole, square]

def move_piece(piece, x, y):
    return set((px + x, py + y) for px,py in piece)

def sim_step(board, piece, tick):
    push_dir = inp[tick % len(inp)]
    dx = { '<': -1, '>': 1}[push_dir]
    pushed_piece = move_piece(piece, dx, 0)
    if min(x for x,y in pushed_piece) < 0:pushed_piece = piece
    if max(x for x,y in pushed_piece) >= 7: pushed_piece = piece
    if len(pushed_piece.intersection(board)) > 0: pushed_piece = piece
    fallen_piece = move_piece(pushed_piece, 0, -1)
    if len(fallen_piece.intersection(board)) > 0:
        return board.union(pushed_piece), None
    if min(y for x,y in fallen_piece) <= 0:
        return board.union(pushed_piece), None
    return board, fallen_piece

def show_board(board, piece, top):
    rows = []
    for y in reversed(range(top)):
        row = []
        if y == 0:
            rows.append("-------")
            continue
        for x in range(7):
            p = (x,y)
            if p in board: row.append('#')
            elif piece is not None and p in piece: row.append('@')
            else: row.append('.')
        rows.append("".join(row))
    return "\n".join(rows)

def sim_rock(board, piece, tick):
    current = deepcopy(shapes[piece % len(shapes)])
    current = move_piece(current, 2, 4 + max((y for (x,y) in board), default=0))
    while current != None:
        board, current = sim_step(board, current, tick)
        tick += 1
    return board, tick

def part1(target):
    tick = 0
    board = set()
    for piece in range(target):
        board, tick = sim_rock(board, piece, tick)
    return max((y for x,y in board), default=0)


def part2(target):
    piece = 0
    tick = 0
    board = set()
    height = 0
    cutpoint = {}
    while piece != target:
        board, tick = sim_rock(board, piece, tick)
        maxy = max((y for x,y in board), default=0)
        for checky in range(maxy-10, maxy):
            if all(((x,checky) in board for x in range(7))):
                print(checky, piece)
                board = {(x,y-checky) for x,y in board if y > checky}
                if checky in cutpoint:
                    print('cycle found')

                    cycle_length = piece - cutpoint[checky][0]
                    skip_cycles = (target - piece) // cycle_length

                    piece += (skip_cycles) * (piece - cutpoint[checky][0])
                    height += (skip_cycles) * (height - cutpoint[checky][1])
                    tick += (skip_cycles) * (tick - cutpoint[checky][2])
                    cutpoint = {}
                cutpoint[checky] = (piece, height, tick)
                height += checky
                continue
        piece += 1
    print(height + max((y for x,y in board), default=0))

if __name__ == '__main__':
    inp = open(sys.argv[1]).read().strip()
    print(part1(2022))
    part2(1000000000000)