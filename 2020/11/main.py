#!/bin/env pypy

from copy import deepcopy
import sys

dirs = [(y,x) for x in [0,1,-1] for y in [0,1,-1]][1:]
def inb(space, y, x): return y in range(len(space)) and x in range(len(space[y]))

def direct(space, y, x):
    return [space[y+a][x+b] for a,b in dirs if inb(space, y+a, x+b)]

def remote(space, y, x):
    n = []
    for a,b in dirs:
        py, px = a+y, b+x
        while inb(space, py, px):
            loc = space[py][px]
            if loc == "L" or loc == "#":
                n.append(loc)
                break
            py, px = py+a, px+b
    return n

def step(source, sees, switch):
    target = deepcopy(source)
    for y, row in enumerate(source):
        for x, seat in enumerate(row):
            occ = sees(source, y, x).count('#')
            if seat == "L" and occ == 0: target[y][x] = "#"
            if seat == "#" and occ >= switch: target[y][x] = "L"
    return target

def sim(source, sees, switch):
    target = step(source, sees, switch)
    while source != target:
        source, target = target, step(target, sees, switch)
    return source

if __name__ == '__main__':
    space = [list(line) for line in open(sys.argv[1])]
    print(sum(row.count("#") for row in sim(space, direct, 4)))
    print(sum(row.count("#") for row in sim(space, remote, 5)))
