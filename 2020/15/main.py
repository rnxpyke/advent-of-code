#!/bin/env python3

def seq(nums, t):
    mem = { n: i+1 for i, n in enumerate(nums)}
    s1, s0 = nums[-2], nums[-1]
    for i in range(len(nums)+1, t+1):
        mem[s1], s1 = i-2, s0
        s0 = i-1-mem[s0] if s0 in mem else 0
    return s0

if __name__ == '__main__':
    starting = [11,18,0,20,1,7,16]
    print(seq(starting, 2020))
    print(seq(starting, 30000000))
