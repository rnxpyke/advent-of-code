#!/usr/bin/env python3

import sys

def parse(line):
   line = line.split('(')
   line[0] = line[0].split(' ')[:-1]
   line[1] = line[1][9:-2]
   line[1] = line[1].split(', ')
   return line

if __name__ == '__main__':
    ingr = open(sys.argv[1]).readlines()
    ingredients = [parse(i) for i in ingr]
    allergenes = []
    for i in ingredients:
        for a in i[1]:
            allergenes.append(a)
    allergenes = set(allergenes)
    dicta = {}
    for i in ingredients:
        for a in i[1]:
            if a not in dicta:
                dicta[a] =[x for x in i[0]]
            else:
                copy = [x for x in dicta[a]]
                for w in copy:
                    if w not in i[0]:
                        dicta[a].remove(w)

    count = 0
    for i in ingredients:
        for n in i[0]:
            b = True 
            for v in dicta.values():
                if n in v:
                   b = False 
            if b:
                count += 1
    print(count)

    dicta = {k:v for k,v in sorted(dicta.items(),key = lambda i: i[0])}
    for w in range(len(dicta)):
        for a, i in dicta.items():
            if len(i) == 1:
                for b, j in dicta.items():
                    if b != a and i[0] in j:
                        j.remove(i[0])
    for k, v in dicta.items():
        dicta[k] = v[0]
    print(','.join(dicta.values()))
