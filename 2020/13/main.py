#!/bin/env python3

import sys

if __name__ == '__main__':
    num, busses = open(sys.argv[1])
    schedule = { i:int(x) for i,x in enumerate(busses.split(",")) if (x != "x") }
    wait, bus = min((-int(num) % bus, bus) for _, bus in schedule.items()) 
    print(wait * bus)

    time, period = 0, 1
    for wait, bus in schedule.items():
        while -time % bus != wait % bus:
            time += period
        period *= bus
    print(time)
