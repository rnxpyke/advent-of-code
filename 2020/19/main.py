#!/usr/bin/env python3

from functools import cache
import regex as re
import sys

def parse_rules(lines):
    rules =  {}
    for line in lines:
        rule, pattern = line.split(':')
        alternatives = pattern.split('|')
        rules[int(rule)] = [a.strip(' ').split(' ') for a in alternatives]
    return rules

@cache
def expr(rule):
    global rules
    if type(rule) is str: 
        try: rule = int(rule)
        except ValueError: return rule[1]
    if rule == 8: return "(?:" + expr(42) + ")+"
    if rule == 11: return "(" + expr(42)  + "(?1)?" + expr(31) + ")"
    pattern = rules[rule]
    res = ["".join(expr(dep) for dep in alt) for alt in pattern]
    if len(res) == 1: return res[0]
    return "(?:" + "|".join(res) + ")"

def matches(pattern, line):
    if pattern.match(line) is None: return False
    return True

if __name__ == '__main__':
    rules, messages = open(sys.argv[1]).read().split('\n\n')
    rules = parse_rules(rules.split('\n'))
    messages = messages.split('\n')
    print(len(messages))
    pattern = '^' + expr(0) + '$'
    test = re.compile(pattern)
    print(sum(matches(test, line) for line in messages))
