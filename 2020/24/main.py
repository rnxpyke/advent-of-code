#!/usr/bin/env python3

import sys

def neighbors(x,y,z):
    r = []
    try: r.append(floor[x+1,y-1,z])
    except KeyError: pass 
    try: r.append(floor[x-1,y+1,z])
    except KeyError: pass
    try: r.append(floor[x+1,y,z-1])
    except KeyError: pass
    try: r.append(floor[x-1,y,z+1])
    except KeyError: pass
    try: r.append(floor[x,y-1,z+1])
    except KeyError: pass
    try: r.append(floor[x,y+1,z-1])
    except KeyError: pass
    return r

def day(floor):
    copy = {}
    for (x,y,z) in floor:
        copy[(x,y,z)] = floor[(x,y,z)]
        c = sum([i%2 for i in neighbors(x,y,z)])
        if floor[(x,y,z)] % 2 == 1 and (c == 0 or c > 2):
            copy[(x,y,z)] += 1
        if floor[(x,y,z)] % 2 == 0 and c == 2:
            copy[(x,y,z)] += 1
    return copy


if __name__ == '__main__':
    lines = open(sys.argv[1]).readlines()
    for i, line in enumerate(lines):
        lines[i] = list(line[:-1])

    for line in lines:
        for j, b in enumerate(line):
            if b == 'n' or b == 's':
                line[j] = line[j] + line.pop(j+1)
    
    floor = {}
    for x in range(-120, 121):
        for y in range(-120,121):
            z = -(x+y)
            floor[(x,y,z)] = 0
    for line in lines:
        x, y, z = 0, 0, 0
        for d in line:
            if d == 'e':
                x += 1
                y -= 1
            if d == 'w':
                x -= 1
                y += 1
            if d == 'ne':
                x += 1
                z -= 1
            if d == 'nw':
                y += 1
                z -= 1
            if d == 'se':
                y -= 1
                z += 1
            if d == 'sw':
                x -= 1
                z += 1
        if (x,y,z) in floor:
            floor[(x,y,z)] += 1
        else:
            floor[(x,y,z)] = 1
    count = 0
    for a in floor.values():
        if a%2 == 1:
            count += 1

    print(count)

    for _ in range(100):
        floor = day(floor)

    count = 0
    for a in floor.values():
        if a%2 == 1:
            count += 1
    print(count)
