#!/usr/bin/env python3

import sys

if __name__ == '__main__':
    keys = [int(line) for line in open(sys.argv[1])]
    mod = 20201227
    loop = [None,None]
    for n in range(mod):
        if pow(7, n, mod) == keys[0]:
            loop[0] = n
            if loop[1] is not None: break
        if pow(7, n, mod) == keys[1]:
            loop[1] = n
            if loop[0] is not None: break
    encryption1 = pow(keys[0], loop[1], mod)
    encryption2 = pow(keys[1], loop[0], mod)
    print(encryption1, encryption2)
