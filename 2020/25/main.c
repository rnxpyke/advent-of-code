#include <stdio.h>
#include <assert.h>

#define int long int
#define CARD 7573546
#define DOOR 17786549
#define MOD  20201227

int crack(int subject, int mod, int target) {
	int acc = 1;
	for (int i = 0; i < mod; i++) {
		if (acc == target) {
			return i;
		}
		acc *= subject;
		acc %= mod;
	}
	return -1;
}

int modpow(int base, int pow, int mod) {
	int acc = 1;
	for (int i = 0; i < pow; i++) {
		acc *= base;
		acc %= mod;
	}
	return acc;
}

int main() {
	int card_loop = crack(7, MOD, CARD);
	int door_loop = crack(7, MOD, DOOR);

	int door_secret = modpow(CARD, door_loop, MOD);
	int card_secret = modpow(DOOR, card_loop, MOD);
	// assert(door_secret == card_secret);

	printf("%d\n", door_secret);
	return 0;
}
