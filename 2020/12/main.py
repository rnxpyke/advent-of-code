#!/bin/env python3

import sys

def move_ship(off, ship, wp): return ship + off, wp
def move_wp(off, ship, wp):   return ship, wp + off

card,rot = { 'N': 1j, 'E': 1+0j, 'S': -1j, 'W': -1+0j }, {'L': 1j, 'R': -1j}
def sim(dirs, ship, wp, move):
    for instr, count in dirs:
        if instr in card: ship, wp = move(count * card[instr], ship, wp)
        if instr in rot:  wp      *= rot[instr]**(count//90)
        if instr == 'F':  ship    += count * wp
    return int(abs(ship.real) + abs(ship.imag))

if __name__ == '__main__':
    dirs = [(l[0], int(l[1:])) for l in open(sys.argv[1])]
    print(sim(dirs, 0+0j, 1+0j, move_ship))
    print(sim(dirs, 0+0j, 10+1j, move_wp))
