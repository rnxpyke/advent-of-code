#!/bin/env python3

from typing import Tuple, Dict, List, Optional
import re
import sys

Bag = str
Contents = List[Tuple[int, Bag]]
Rules = Dict[Bag, Contents]

def parse(line: str) -> Optional[Tuple[Bag, Contents]]:
    m = re.search(r"(.*?) bags contain", line)
    if m is None: return None
    matches = re.findall(r"(\d+?) (.*?) bag", line)
    values = [(int(n), b) for n,b in matches or []]
    return (m.group(1), values)

def contains_gold(dict: Rules, color: Bag) -> bool:
    if color not in dict: return False
    for (num, bag) in dict[color]:
        if bag == "shiny gold": return True
        if contains_gold(dict , bag): return True
    return False

def num_bags(dict: Rules, color: Bag) -> int:
    if color not in dict: return 0
    total = 0
    for (num, bag) in dict[color]:
        total += num * (1 + num_bags(dict, bag))
    return total


if __name__ == '__main__':
    filePath = sys.argv[1]
    with open(filePath, 'r') as f:
        mappings = (m for l in f.readlines() if (m := parse(l)) is not None)
        dict = { c: v for c,v in mappings }
    
    print(sum(contains_gold(dict, bag) for bag in dict ))
    print(num_bags(dict, "shiny gold"))
