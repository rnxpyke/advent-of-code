package main

import (
	"bufio"
	"fmt"
	"io"
	"math/bits"
	"os"
)

func read_input(input io.Reader) (groups [][]uint, err error) {
	var answers []uint
	scanner := bufio.NewScanner(input)
	for scanner.Scan() {
		if bits := bitfield(scanner.Text()); bits == 0 {
			groups, answers = append(groups, answers), []uint{}
		} else {
			answers = append(answers, bits)
		}
	}
	return append(groups, answers), scanner.Err()
}

func bitfield(s string) (res uint) {
	for _, rune := range s {
		res = res | (1 << (rune - 'a'))
	}
	return res & (1<<27 - 1) // mask non alphabet chars
}

func diff(a, b uint) uint  { return a & b }
func union(a, b uint) uint { return a | b }
func count(groups [][]uint, f func(a, b uint) uint, init uint) (total int) {
	for _, group := range groups {
		set := init
		for _, answer := range group {
			set = f(set, answer)
		}
		total += bits.OnesCount(set)
	}
	return
}

func main() {
	file, _ := os.Open(os.Args[1])
	defer file.Close()
	groups, _ := read_input(file)
	fmt.Println(count(groups, union, bitfield("")))
	fmt.Println(count(groups, diff, bitfield("abcdefghijklmnopqrstuvwxyz")))
}
