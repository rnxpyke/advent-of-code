:- use_module(library(pcre)).

string_set(String, Set) :- string_chars(String, L), list_to_set(L, Set).

assert_group(G) :-
	split_string(G,'\n', '\r', Answers),
	findall(Set, (member(Answer, Answers), \+ Answer = "", string_set(Answer, Set)), Res),
	\+ Res = [],
	assertz(groups(Res)).
assert_group(_) :- true.

init :-
	open('input.txt', read, S), read_string(S, _, Input),
	re_split('\n\n', Input, Groups, [newline(lf)]),
	forall(member(Group, Groups), assert_group(Group)).

:- init.

group_all(G, Count) :-
	foldl(union, G, [], Res),
	length(Res, Count).

group_same(G, Count) :-
	string_set("abcdefghijklmnopqrstuvwxyz", S),
	foldl(intersection, G, S, Res),
	length(Res, Count).

sol_first(Result) :-
	findall(Count, (groups(G), group_all(G, Count)), Counts),
	sumlist(Counts, Result).

sol_second(Result) :-
	findall(Count, (groups(G), group_same(G, Count)), Counts),
	sumlist(Counts, Result).


main :-
	sol_first(A), writeln(A),
	sol_second(B), writeln(B),
	halt.
