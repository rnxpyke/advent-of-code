#!/usr/bin/env python3

import sys

def play(player1, player2):
    while len(player1) > 0 and len(player2) > 0:
        c1, c2 = player1[0], player2[0]
        player1, player2 = player1[1:], player2[1:]
        if c1 > c2:
            player1.append(c1)
            player1.append(c2)
        else:
            player2.append(c2)
            player2.append(c1)
    return player1, player2

def rec(a,b):
    mem = {}
    while len(a) > 0 and len(b) > 0:
        if (str(a),str(b)) in mem: return 0, (a,b)
        mem[(str(a),str(b))] = None
        ta, tb = a[0], b[0]
        na, nb = [x for x in a[1:]], [x for x in b[1:]]
        if ta > len(na) or tb > len(nb):
            winner = int(tb > ta)
        else:
            winner, _ = rec(na[:ta], nb[:tb])
        if bool(winner):
            nb.append(tb)
            nb.append(ta)
        else:
            na.append(ta)
            na.append(tb)
        a,b = na,nb
    return len(a) == 0, (a,b)
    

def score(end1,end2):
    winner = end1 if len(end1) > 0 else end2
    winner.reverse()
    return sum(map(lambda t: t[0]*t[1], enumerate(winner, 1)))

if __name__ == '__main__':
    decks = open(sys.argv[1]).read().split('\n\n')
    decks = [[int(line) for line in deck.split('\n')[1:] if line != ""] for deck in decks]
    print(score(*play(*decks)))
    winner, res = rec(*decks)
    print(score(*res))
