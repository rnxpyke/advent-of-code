#!/usr/bin/env python3

import sys

def destination(v, l): return (v - 2) % l + 1
def move(cycle, current):
    a = cycle[current]
    b = cycle[a]
    c = cycle[b]
    dest = destination(current, len(cycle))
    if dest in [a,b,c]: dest = destination(dest, len(cycle))
    if dest in [a,b,c]: dest = destination(dest, len(cycle))
    if dest in [a,b,c]: dest = destination(dest, len(cycle))
    cycle[current] = cycle[c]
    cycle[c] = cycle[dest]
    cycle[dest] = a

def score(cycle):
    cups = []
    current = 1
    for _ in cycle:
        current = cycle[current]
        cups.append(current)
    return "".join(str(cup) for cup in cups[:-1])

def solve(cups, num_moves):
    # Current Cup is first cup in list
    current = cups[0]
    # Cycle[cup_value] = next_cup_value in clockwise order
    cycle = {}
    for i, cup in enumerate(cups, 1):
        cycle[int(cup)] = cups[i % len(cups)]
    for _ in range(num_moves):
        move(cycle, current)
        current = cycle[current]
    return cycle

if __name__ == '__main__':
    cups = [int(i) for i in list(sys.argv[1])]
    print(score(solve(cups, 100)))
   

    million = 1000000
    new_cups = cups + [x for x in range(10, million+1)]
    cycle = solve(new_cups, 10*million)
    a = cycle[1]
    b = cycle[a]
    print(a*b)
     
