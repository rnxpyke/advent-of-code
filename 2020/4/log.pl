:- use_module(library(pcre)).

% used in parseport to build up passport dictonary
goal(Match, Acc, Out) :-
	_{key: KS, value: V} :< Match, !,
	atom_string(K, KS),
   	Out = Acc.put(K, V).

% convert passport entrys to dictonary. Can't be empty.
parseport(Pass, Port) :-
	re_foldl(goal, '(?P<key>\\w+):(?P<value>\\S+)', Pass, passport{}, Port, [newline(lf)]),
	\+ Port = passport{}.

% read from input
init :-
	open('input4.txt', read, S),
	read_string(S, _, Input),
	re_split('\n\n', Input, Passports, [newline(lf)]),
	forall((member(Passport, Passports), parseport(Passport, Port)), assertz(doc(Port))).
:- init.

valid_keys(Port) :-	passport{byr:_, ecl:_, eyr:_, hcl:_, hgt:_, iyr:_, pid:_ } :< Port, !.
pkeys(Port) :- doc(Port), valid_keys(Port). 

string4_low_high(String, Low, High) :-
		re_match("^\\d{4}$", String),
		number_string(Num, String),
		between(Low, High, Num).

valid_height(S) :- string_concat(N, "cm", S), number_string(H, N), between(150, 193, H).
valid_height(S) :- string_concat(N, "in", S), number_string(H, N), between(59, 76, H).

valid(Port) :- pkeys(Port),
	string4_low_high(Port.byr, 1920, 2002),
	string4_low_high(Port.iyr, 2010, 2020),
	string4_low_high(Port.eyr, 2020, 2030),
	valid_height(Port.hgt),
	re_match("^#[0-9a-f]{6}$", Port.hcl),
	re_match("^(amb|blu|brn|gry|grn|hzl|oth)$", Port.ecl),
	re_match("^\\d{9}$", Port.pid).

main :-
	aggregate(count, P^pkeys(P), Count), writeln(Count),
	aggregate(count, X^valid(X), Xount), writeln(Xount),
	halt.
