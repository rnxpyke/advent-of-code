#!/bin/env python3

import re
import sys

# returns the data of a passport as a python dictonary
def passportData(string):
    pattern = re.compile(r"(\w+):(\S+)")
    return { key: value for (key,value) in pattern.findall(string)}

# validate diffrent height schemes
def validHeight(height):
    match = re.compile(r"^(\d+)(cm|in)$").search(height)
    if match is None: return False
    num, unit = match.groups()
    if unit == 'cm': return 150 <= int(num) <= 193
    if unit == 'in': return 59 <= int(num) <= 76

validator = {
    'byr': lambda year: 1920 <= int(year) <= 2002 and len(year) == 4,
    'iyr': lambda year: 2010 <= int(year) <= 2020 and len(year) == 4,
    'eyr': lambda year: 2020 <= int(year) <= 2030 and len(year) == 4,
    'hgt': validHeight,
    'hcl': lambda col: re.compile(r"^#([0-9]|[a-f]){6}$").match(col),
    'ecl': lambda col: re.compile(r"^(amb|blu|brn|gry|grn|hzl|oth)$").match(col),
    'pid': lambda id: re.compile(r"^\d{9}$").match(id),
    # 'cid': this is ignored on purpose
}

def contains(port):
    # all keys that the validator expects are in the passport
    return all(key in port for key in validator)

def valid(port):
    # all necessary keys are present *and* valid, according to validator
    return all(key in port and valid(port[key]) for (key, valid) in validator.items())

if __name__ == '__main__':
    filePath = sys.argv[1]
    with open(filePath, 'r') as f:
        passStrings = f.read().split('\n\n')
        passports = list(map(passportData, passStrings))
    print(sum(contains(port) for port in passports), 'passports contain the necessary keys')
    print(sum(valid(port) for port in passports), 'passports are valid')
