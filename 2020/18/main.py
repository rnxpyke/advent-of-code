#!/usr/bin/env python3

import sys
import parsimonious as pars

rules = r"""
    root = expr _
    expr = operand (_ op _ operand)+
    operand = number / paren 
    op = "+" / "*"
    paren = "(" expr ")"
    number = ~r"\d+"
    _ = (~r"\s+")*
"""

rules2 = r"""
    root = expr _
    expr = factor (_ times _ factor)*
    factor = term (_ plus _ factor)*
    term = paren / number
    times = "*"
    plus = "+"
    number = ~r"\d+"
    paren = "(" expr ")"
    _ = (~r"\s+")*
"""
grammar = pars.grammar.Grammar(rules)
grammar2 = pars.grammar.Grammar(rules2)

def foldl(s):
    if len(s) <= 1 or s[1] is None: return s[0]
    for _, op, _, num in s[1]: s[0] = s[0] * num if op == "*" else s[0] + num
    return s[0]

class Reduce(pars.NodeVisitor):
    def visit_root(self, node, children): return children[0]
    def visit_expr(self, node, children): return foldl(children)
    def visit_factor(self, node, children): return foldl(children)
    def visit_operand(self, node, children): return children[0]
    def visit_op(self, node, children): return node.text
    def visit_term(self, node, children): return children[0]
    def visit_paren(self, node, children): return children[1]
    def visit_number(self, node, children): return int(node.text)
    def visit_times(self, node, children): return node.text
    def visit_plus(self, node, children): return node.text
    def visit__(self, node, children): return None
    def visit_(self, node, children): return children

if __name__ == '__main__':
    lines = [l.rstrip('\n') for l in open(sys.argv[1])]
    print(sum(Reduce().visit(grammar.parse(line)) for line in lines))
    print(sum(Reduce().visit(grammar2.parse(line)) for line in lines))

