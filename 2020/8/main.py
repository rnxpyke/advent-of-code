#!/bin/env python3

import re
import sys
from typing import Tuple, Optional, NewType, List, Generator

PC = int
Opcode = str
Instr = Tuple[Opcode, int]
Program = List[Instr]
State = Tuple[PC, int]

def decode(line: str) -> Optional[Instr]:
    m = re.search(r'(jmp|acc|nop) ((?:\+|-)[0-9]+)', line)
    if m is None: return None
    return (m.group(1), int(m.group(2)))

def step(program: Program, pc: PC, acc: int) -> Optional[State]:
    if pc >= len(program): return None
    op, arg = program[pc]
    if op == 'nop': return pc + 1, acc
    if op == 'acc': return pc + 1, acc + arg
    if op == 'jmp': return pc + arg, acc
    return None

def run(program: Program) -> Generator[State, None, None]:
    state: Optional[State] = (PC(0), 0)
    while state is not None:
        yield state
        state = step(program, *state)

def first_loop(program) -> Optional[int]:
    visited = [False for instr in program]
    for pc, acc in run(program):
        if pc >= len(visited): return None
        if visited[pc] == True: return acc
        visited[pc] = True

def swap(op: Opcode) -> Opcode: return 'jmp' if op == 'nop' else 'nop'
def fix(program) -> Optional[int]:
    for i, (op, arg) in enumerate(program):
        if op in ['jmp', 'nop']:
            program[i] = (swap(op), arg)
            if first_loop(program) is None:
                # destructures iterator into [x_1, x_2, ...], x_n
                *_, (pc, acc) = run(program)
                return acc
            program[i] = (op, arg)

if __name__ == '__main__':
    with open(sys.argv[1]) as f:
        program = [i for l in f.readlines() if (i := decode(l)) is not None]
    print(first_loop(program))
    print(fix(program))
    
