#!/bin/env python3

import sys

def diffs(data):
    diffs = [b-a for (a,b) in zip(data, data[1:])]
    return diffs.count(1) * diffs.count(3)

def configs(data, target):
    memo = [1] + [0] * max(data)
    for n in data[1:]:
        memo[n] = sum(memo[n-a] for a in [1,2,3])
    return memo[target]

if __name__ == '__main__':
    data = [int(n) for n in open(sys.argv[1])]
    data = sorted(data + [0, max(data) + 3])
    print(diffs(data))
    print(configs(data, max(data)))
