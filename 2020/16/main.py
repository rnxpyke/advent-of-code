#!/bin/env python3

import sys
import re
from math import prod

def make_range(l, h, L, H): return range(l,h+1), range(L,H+1)
def parse(f):
    a,b,c = f.split("\n\n")
    ticket, rule = r'(?:\d+,)+(?:\d+)', r'(.*): (\d+)-(\d+) or (\d+)-(\d+)'
    rules = re.findall(rule, a)
    rules = { key: make_range(*map(int, rest)) for key,*rest in rules }
    tickets = [list(map(int, t.split(","))) for t in re.findall(ticket, c)]
    return rules, re.findall(r'\d+', b), tickets

def matches(rule, v): return v in rule[0] or v in rule[1]
def any_rule(rules, v): return any(matches(r, v) for r in rules.values())
def valid(rules, ticket): return all(any_rule(rules, v) for v in ticket)
def err_values(rules, t): return sum(v for v in t if not any_rule(rules, v))
def err_rate(rules, tickets): return sum(err_values(rules, t) for t in tickets)


def rule_all(rule, ts, i): return all(matches(rule, t[i]) for t in ts)
def possible(r, ts): return [i for i in range(len(ts[0])) if rule_all(r, ts, i)]
def mapping(rs, ts):
    m = {k: possible(r, ts) for k,r in rs.items()}
    m = {k:v for k,v in sorted(m.items(), key=lambda i: len(i[1]))}
    mapping = {}
    for rule, poss in m.items():
        mapping[rule] = next(filter(lambda i: i not in mapping.values(), poss))
    return mapping

if __name__ == '__main__':
    rules, my, tickets = parse(open(sys.argv[1]).read())
    print(err_rate(rules, tickets))
    tickets = [t for t in tickets if valid(rules, t)]
    matches = mapping(rules, tickets)
    print(prod(int(my[i]) for m, i in matches.items() if 'departure' in m))
