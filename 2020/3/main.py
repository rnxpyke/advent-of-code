#!/bin/env python3

from math import prod
import sys

def diag(terrain, dx, dy):
    x,y = (0,0)
    while True:
        try:
            yield terrain[y][x]
            x = (x + dx) % len(terrain[0])
            y += dy
        except IndexError:
            break

def numTrees(terrain, dx, dy):
    return sum(place == '#' for place in diag(terrain, dx, dy))

if __name__ == '__main__':
    filePath = sys.argv[1]
    with open(filePath, 'r') as f:
        terrain = f.read().split('\n')
    print(numTrees(terrain, 3, 1), 'trees on first slope')
    
    slopes = [(1,1), (3,1), (5,1), (7,1), (1,2)]
    trees = list(numTrees(terrain, *slope) for slope in slopes)
    print(prod(trees), 'is the product of trees on different slopes')
