:- use_module(library(pcre)).

read_file(Stream, X) :- \+ at_end_of_stream(Stream), read_string(Stream, '\n', '\r', _, X).
read_file(Stream, X) :- \+ at_end_of_stream(Stream), read_file(Stream, X).


fblr_bin(String, Bin) :-
	re_replace('F|L'/g, '0', String, X),
	re_replace('B|R'/g, '1', X, Bin).


seat_index(Seat, Index) :-
	fblr_bin(Seat, BinString),
	string_concat('0b', BinString, Bin),
	number_string(Index, Bin).

init :-
	open('input5.txt', read, S),
	forall((
		read_file(S, Seat), 
		seat_index(Seat, Index)
	), assertz(seat(Index))).
:- init.


test :-
	seat_index("BFFFBBFRRR", 567),
	seat_index("FFFBBBFRRR", 119),
	seat_index("BBFFBBFRLL", 820).

missing(Pass) :-
	aggregate(max(M), seat(M), Highest),
	aggregate(min(M), seat(M), Lowest),
	between(Lowest, Highest, V),
	\+ seat(V), Pass = V.

main :-
	test,
	aggregate(max(M), seat(M), Highest),
	writeln(Highest),
	missing(Pass), writeln(Pass),
	halt.

