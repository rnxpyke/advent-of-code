#!/bin/env python3

from itertools import combinations
from math import prod
import sys

if __name__ == '__main__':
    nums = [int(line) for line in open(sys.argv[1])]

    print(next(prod(x) for x in combinations(nums, 2) if sum(x) == 2020))
    print(next(prod(x) for x in combinations(nums, 3) if sum(x) == 2020))
