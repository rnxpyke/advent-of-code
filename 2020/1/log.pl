:- use_module(library(writef)).

read_file(Stream,  X) :- \+ at_end_of_stream(Stream), read_string(Stream, '\n', '\r', _, X).
read_file(Stream,  X) :- \+ at_end_of_stream(Stream), read_file(Stream, X).

%assert all numbers using backtracking on read_file
init :-
	open('input.txt', read, Str),
	read_file(Str, S),
	number_string(N, S),
	assertz(numbers(N)),
	fail.
init :- true.
:- init.


solution(A,B) :-
	numbers(A), 
	numbers(B),
	A =< B,
	plus(A,B,2020).

solution(A,B,C) :-
	numbers(A),
   	numbers(B),
	A =< B,
	numbers(C),
	B =< C,
	sumlist([A, B, C], 2020).

main :-
	solution(X,Y),
	First is X * Y,
	writef("%w %w: Product %w\n", [X, Y, First]),
	solution(A, B, C),
	Second is A * B * C,
	writef("%w %w %w: Product %w\n", [A, B, C, Second]),
	halt.

