#!/usr/bin/env python3

import sys
import numpy as np

def toBits(l): return sum(int(b=='#') * (2**i) for i,b in enumerate(l))
def allSides(tile):
    a,b,c,d = tile[0, :   ], tile[-1, :   ], tile[:,    0],    tile[:, -1]
    e,f,g,h = tile[0, ::-1], tile[-1, ::-1], tile[::-1, 0], tile[::-1, -1]
    return [a,b,c,d,e,f,g,h]

if __name__ == '__main__':
    tilestrings = open(sys.argv[1]).read().split('\n\n')
    tiles = {}
    for tile in tilestrings:
        tid, *data = tile.split('\n')
        try:
            tid = tid.rstrip(':').split(' ')[1]
        except IndexError: continue
        grid = [[c for c in line] for line in data if len(line) >= 1]
        tiles[int(tid)] = np.array(grid)
  
    sides = {}
    for tid, tile in tiles.items():
        for x in allSides(tile):
            bits = toBits(x)
            if bits not in sides: sides[bits] = []
            sides[bits].append(tid)
    sides = {k:v for k,v in sorted(sides.items(), key=lambda i: len(i[1]))}
    
    singles = {}
    for bits, xs in sides.items():
        if len(xs) == 1:
            singles[xs[0]] = []
    for sid in singles.keys():
        matches = [x for side in allSides(tiles[sid]) for x in sides[toBits(side)]]
        singles[sid]  = list(dict.fromkeys(matches))
    prod = 1
    for sid, single in singles.items():
        if len(single) < 4:
            print(sid, single)
            prod *= sid
    print(prod)
