#!/bin/env python3

import sys

def cluster(n, data):
    prev = []
    for element in data:
        if n <= len(prev):
            yield (element, prev)
            prev = prev[1:]
        prev.append(element)

def sum_of(e, data):
    for x in data:
        for y in data:
            if x + y == e: return True
    return False

def contigious(total, data):
    start = 0
    end = -1
    total = 0
    for i, value in enumerate(data):
        total += value
        end = i+1
        while total > invalid:
            total -= data[start]
            start += 1
        if total == invalid:
            return data[start:end]

if __name__ == '__main__':
    data = [int(line) for line in open(sys.argv[1])]
    invalid = None
    for e, prev in cluster(25, data):
        if not sum_of(e, prev): invalid = e
    print(invalid)
    weak = contigious(invalid, data)
    print(min(weak) + max(weak))
