#!/bin/env python

import re
import sys

def parse(line):
    p = r'(mask = (?P<m>[01X]+))|(mem\[(?P<a>\d+)\] = (?P<v>\d+))'
    return re.search(p, line).groupdict()

def one(addr, mask, x, value): yield addr, (value & x) | (mask & ~x)
def many(addr, mask, x, value, comb=0):
    for _ in range(2**bin(x).count("1")):
        yield comb | mask | (addr & ~x), value
        comb = ((comb | ~x) + 1) & x

def sim(prog, store, mem = {}, mask = 0, x = 0):
    for inst in prog:
        if inst['m'] is not None:
            mask = int(inst['m'].replace('X', '0'), 2)
            x = int(inst['m'].replace('1', '0').replace('X', '1'), 2)
            continue
        for a, v in store(int(inst['a']), mask, x, int(inst['v'])): mem[a] = v
    return sum(mem.values())

if __name__ == '__main__':
    instr = [parse(x) for x in open(sys.argv[1])]
    print(sim(instr, one, mem={}))
    print(sim(instr, many, mem={}))
